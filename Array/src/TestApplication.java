import it.oop.polito.bigarray.BigArray;

public class TestApplication {
	public static void main(String[] args) {
		BigArray b = new BigArray(50);

		b.setElement(23, 10);
		b.setElement(10, 23);

		System.out.println("10: " + b.getElement(10));
		System.out.println("18: " + b.getElement(18));
		System.out.println("23: " + b.getElement(23));

		b.setElement(1800, 500);

		System.out.println("1000: " + b.getElement(1000));
		System.out.println("1800: " + b.getElement(1800));
		System.out.println("2300: " + b.getElement(2300));
		System.out.println("10: " + b.getElement(10));
		System.out.println("18: " + b.getElement(18));
		System.out.println("23: " + b.getElement(23));

	}
}
