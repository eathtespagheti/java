package it.oop.polito.bigarray;

public class BigArray {
	private int[] arrayData;

	public BigArray() {
		super();
		this.arrayData = new int[100];
	}

	public BigArray(int size) {
		super();
		this.arrayData = new int[size];
	}

	public void setElement(int index, int value) {
		if (index >= this.arrayData.length) {
			return;
		}
		this.arrayData[index] = value;
	}

	public int getElement(int index) {
		if (index >= this.arrayData.length || index < 0) {
			return 0;
		}
		return this.arrayData[index];
	}

}
