public class Person {
    String Name;
    Boolean Happy;

    Person() {
        this.Name = "Name";
        this.Happy = false;
    }

    Person(String Name) {
        this.Name = Name;
        this.Happy = false;
    }

    Person(String Name, Boolean Happy) {
        this.Name = Name;
        this.Happy = Happy;
    }

    void greet() {
        System.out.print("Hello I'm " + this.Name);
        if (this.Happy) {
            System.out.print(" and I'm happy!");
        }
        System.out.print("\n");
    }

    void tellJoke() {
        this.Happy = true;
    }
}