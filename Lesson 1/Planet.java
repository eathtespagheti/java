public class Planet {
    String Name;
    double OrbitalRadius;

    void describe() {
        System.out.println("Planet " + this.Name + " with a major semi-axes of " + this.OrbitalRadius + "km");
    }
}