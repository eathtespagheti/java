public class Main {
    public static void main(String[] args) {
        System.out.println("Hello word!");

        Person bob = new Person("Bob");
        bob.greet();
        bob.Happy = true;
        bob.greet();

        Planet mars = new Planet();
        mars.Name = "Mars";
        mars.OrbitalRadius = 5516.43;
        mars.describe();
    }
}