#!/usr/bin/env bash

className=$1
filename=$className.java

touch $filename

echo "public class ${className} {" >>$filename
echo "" >>$filename
echo "}" >>$filename
