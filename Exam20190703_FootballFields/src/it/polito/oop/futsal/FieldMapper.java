/**
 * 
 */
package it.polito.oop.futsal;

import it.polito.oop.futsal.Fields.Features;

/**
 * @author Fabio Sussarellu s251489
 * @email fabio.sussarellu@studenti.polito.it
 *
 */
public class FieldMapper implements FieldOption {
	private Fields reference;
	private Features field;

	/**
	 * @param reference
	 * @param field
	 */
	public FieldMapper(Fields reference, Features field) {
		super();
		this.reference = reference;
		this.field = field;
	}

	@Override
	public int getField() {
		return this.field.getId();
	}

	@Override
	public int getOccupation() {
		return this.reference.getOccupation(this.getField());
	}

}
