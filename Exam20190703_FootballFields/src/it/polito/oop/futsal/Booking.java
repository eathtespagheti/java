/**
 * 
 */
package it.polito.oop.futsal;

import it.polito.oop.futsal.Fields.Features;

/**
 * @author Fabio Sussarellu s251489
 * @email fabio.sussarellu@studenti.polito.it
 *
 */
public class Booking {
	private Features field;
	private Associate associate;
	private Hour time;

	/**
	 * @param field
	 * @param associate
	 * @param time
	 * @throws FutsalException
	 */
	public Booking(Features field, Associate associate, Hour time, Hour referenceOpening, Hour referenceClosing)
			throws FutsalException {
		super();
		Booking.checkTime(time, referenceOpening, referenceClosing);
		this.setField(field);
		this.setAssociate(associate);
		this.setTime(time);
	}

	/**
	 * @param field
	 * @param associate
	 * @param time
	 */
	public Booking(Features field, Associate associate, Hour time) {
		super();
		this.field = field;
		this.associate = associate;
		this.time = time;
	}

	static void checkTime(Hour time, Hour referenceOpening, Hour referenceClosing) throws FutsalException {
		if (!time.getMinutes().equals(referenceOpening.getMinutes())) {
			throw new FutsalException();
		}
		if (time.getHourAsInt() > referenceClosing.getHourAsInt()) {
			throw new FutsalException();
		}
		if (time.getHourAsInt() == referenceClosing.getHourAsInt()
				&& time.getMinutesAsInt() > referenceClosing.getMinutesAsInt()) {
			throw new FutsalException();
		}
	}

	/**
	 * @return the field
	 */
	public Features getField() {
		return field;
	}

	/**
	 * @param field the field to set
	 */
	public void setField(Features field) {
		this.field = field;
	}

	/**
	 * @return the associate
	 */
	public Associate getAssociate() {
		return associate;
	}

	/**
	 * @param associate the associate to set
	 */
	public void setAssociate(Associate associate) {
		this.associate = associate;
	}

	/**
	 * @return the time
	 */
	public Hour getTime() {
		return time;
	}

	/**
	 * @param time the time to set
	 */
	public void setTime(Hour time) {
		this.time = time;
	}

}
