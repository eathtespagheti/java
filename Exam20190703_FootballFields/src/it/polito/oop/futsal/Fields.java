package it.polito.oop.futsal;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Represents a infrastructure with a set of playgrounds, it allows teams to
 * book, use, and leave fields.
 *
 */
public class Fields {
	private int countField = 1;
	private int countAssociates = 1;
	private List<Features> features = new LinkedList<>();
	private Hour openingTime = new Hour("00:00");
	private Hour closingTime = new Hour("00:00");
	private List<Associate> associates = new LinkedList<>();
	private List<Booking> bookings = new LinkedList<>();

	public static class Features extends IDAble {
		public boolean indoor; // otherwise outdoor
		public boolean heating = false;
		public boolean ac = false;

		public Features(boolean i, boolean h, boolean a) {
			super(0);
			this.indoor = i;
			this.heating = h;
			this.ac = a;
		}

		/**
		 * @return the indoor
		 */
		public boolean isIndoor() {
			return indoor;
		}

		/**
		 * @return the heating
		 */
		public boolean isHeating() {
			return heating;
		}

		/**
		 * @return the ac
		 */
		public boolean isAc() {
			return ac;
		}

		public void checkParameters() throws FutsalException {
			if (!this.indoor && (this.heating || this.ac)) {
				throw new FutsalException();
			}
		}

		public boolean checkFeatures(Features f) {
			if (f.indoor && !f.indoor) {
				return false;
			}
			if (f.heating && !f.heating) {
				return false;
			}
			if (f.ac && !f.ac) {
				return false;
			}
			return true;
		}

	}

	public void defineFields(Features... features) throws FutsalException {
		for (Features feature : features) {
			feature.checkParameters();
			feature.setId(this.countField++);
			this.features.add(feature);
		}
	}

	public long countFields() {
		return this.features.size();
	}

	public long countIndoor() {
		return this.features.parallelStream().filter(Features::isIndoor).count();
	}

	/**
	 * @return the openingTime
	 */
	public String getOpeningTime() {
		return this.openingTime.toString();
	}

	/**
	 * @param openingTime the openingTime to set
	 */
	public void setOpeningTime(String openingTime) {
		this.openingTime.setTime(openingTime);
	}

	/**
	 * @return the closingTime
	 */
	public String getClosingTime() {
		return this.closingTime.toString();
	}

	/**
	 * @param closingTime the closingTime to set
	 */
	public void setClosingTime(String closingTime) {
		this.closingTime.setTime(closingTime);
	}

	public int newAssociate(String first, String last, String mobile) {
		Associate tmp = new Associate(first, last, mobile, this.countAssociates++);
		this.associates.add(tmp);
		return tmp.getId();
	}

	private Associate getAssociate(int id) throws FutsalException {
		Optional<Associate> s = this.associates.parallelStream().filter(a -> a.getId().equals(Integer.valueOf(id)))
				.findAny();
		if (!s.isPresent()) {
			throw new FutsalException();
		}
		return s.get();
	}

	public String getFirst(int partyId) throws FutsalException {
		return this.getAssociate(partyId).getName();
	}

	public String getLast(int associate) throws FutsalException {
		return this.getAssociate(associate).getSurname();
	}

	public String getPhone(int associate) throws FutsalException {
		return this.getAssociate(associate).getPhoneNumber();
	}

	public int countAssociates() {
		return this.associates.size();
	}

	private Features getField(int id) throws FutsalException {
		Optional<Features> field = this.features.parallelStream().filter(f -> f.getId().equals(id)).findAny();
		if (!field.isPresent()) {
			throw new FutsalException();
		}
		return field.get();
	}

	public void bookField(int field, int associate, String time) throws FutsalException {
		if (this.isBooked(field, time)) {
			throw new FutsalException();
		}
		this.bookings.add(new Booking(this.getField(field), this.getAssociate(associate), new Hour(time),
				this.openingTime, this.closingTime));
	}

	public boolean isBooked(int field, String time) {
		return this.bookings.parallelStream()
				.anyMatch(b -> b.getField().getId().equals(field) && b.getTime().equals(new Hour(time)));
	}

	public boolean isBooked(Features field, String time) {
		return this.bookings.parallelStream()
				.anyMatch(b -> b.getField().equals(field) && b.getTime().equals(new Hour(time)));
	}

	public int getOccupation(int field) {
		return (int) this.bookings.parallelStream().filter(b -> b.getField().getId().equals(field)).count();
	}

	public List<FieldOption> findOptions(String time, Features required) {
		return this.features.parallelStream().filter(f -> f.checkFeatures(required)).filter(f -> this.isBooked(f, time))
				.map(f -> new FieldMapper(this, f)).collect(Collectors.toList());
	}

	public long countServedAssociates() {
		return this.bookings.parallelStream().map(Booking::getAssociate).distinct().count();
	}

	private Long bookingsPerField(Features f) {
		return this.bookings.parallelStream().filter(b -> b.getField().equals(f)).count();
	}

	public Map<Integer, Long> fieldTurnover() {
		return this.features.parallelStream().collect(Collectors.toMap(IDAble::getId, this::bookingsPerField));
	}

	public double occupation() {
		int intervals = this.closingTime.getHourAsInt() - this.openingTime.getHourAsInt();
		intervals -= this.closingTime.getMinutesAsInt() - this.openingTime.getMinutesAsInt() < 0 ? 1 : 0;
		return ((double) this.bookings.size()) / intervals;
	}

}
