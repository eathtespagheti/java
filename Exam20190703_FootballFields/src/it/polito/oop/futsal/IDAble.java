/**
 * 
 */
package it.polito.oop.futsal;

/**
 * @author Fabio Sussarellu s251489
 * @email fabio.sussarellu@studenti.polito.it
 *
 */
class IDAble {
	private Integer id;

	/**
	 * @param id
	 */
	public IDAble(int id) {
		super();
		this.setId(id);
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

}
