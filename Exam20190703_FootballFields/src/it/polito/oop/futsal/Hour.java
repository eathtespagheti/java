/**
 * 
 */
package it.polito.oop.futsal;

import static org.junit.Assert.assertNotNull;

/**
 * @author Fabio Sussarellu s251489
 * @email fabio.sussarellu@studenti.polito.it
 *
 */
public class Hour {
	private String hour = "00";
	private String minutes = "00";
	static final String SPLITTER = ":";

	/**
	 * 
	 */
	public Hour(String time) {
		super();
		this.setTime(time);
	}

	/**
	 * @param hour
	 * @param minutes
	 */
	public Hour(String hour, String minutes) {
		super();
		this.hour = hour;
		this.minutes = minutes;
	}

	public void setTime(String time) {
		String[] splitted = Hour.parseHour(time);
		this.setHour(splitted[0]);
		this.setMinutes(splitted[1]);
	}

	static String[] parseHour(String timeString) {
		return timeString.split(Hour.SPLITTER);
	}

	/**
	 * @return the hour
	 */
	public String getHour() {
		return hour;
	}

	/**
	 * @return the hour
	 */
	public Integer getHourAsInt() {
		return Integer.parseInt(this.getHour());
	}

	/**
	 * @param hour the hour to set
	 */
	public void setHour(String hour) {
		this.hour = hour;
	}

	/**
	 * @return the minutes
	 */
	public String getMinutes() {
		return minutes;
	}

	/**
	 * @return the minutes
	 */
	public Integer getMinutesAsInt() {
		return Integer.parseInt(this.getMinutes());
	}

	/**
	 * @param minutes the minutes to set
	 */
	public void setMinutes(String minutes) {
		this.minutes = minutes;
	}

	@Override
	public String toString() {
		return this.getHour() + ":" + this.getMinutes();
	}

	@Override
	public boolean equals(Object obj) {
		return this.toString().equals(obj.toString());
	}
}
