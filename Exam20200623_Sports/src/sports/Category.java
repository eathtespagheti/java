package sports;

import java.util.LinkedList;
import java.util.List;

public class Category {
	private String name;
	List<String> activities = new LinkedList<>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getActivities() {
		return activities;
	}

	public void setActivities(List<String> activities) {
		this.activities = activities;
	}

	public Category(String name) {
		super();
		this.name = name;
	}

	public void addActivity(String a) {
		this.activities.add(a);
	}

	public void addActivities(String... a) {
		for (String ac : a) {
			this.addActivity(ac);
		}
	}

	public Category(String name, List<String> activities) {
		super();
		this.name = name;
		this.activities.addAll(activities);
	}

	public boolean containActivity(String a) {
		return this.activities.contains(a);
	}

}
