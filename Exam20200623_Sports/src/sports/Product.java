package sports;

public class Product {
	private String name;
	private String activity;
	private Category category;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Product(String name, String activity, Category category) {
		super();
		this.name = name;
		this.activity = activity;
		this.category = category;
	}

	public boolean sameName(Product p) {
		return this.getName().equals(p.getName());
	}

	public boolean sameName(String pName) {
		return this.getName().equals(pName);
	}

}
