package sports;

import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

/**
 * Facade class for the research evaluation system
 *
 */
public class Sports {
	List<String> activities = new LinkedList<>();
	List<Category> categories = new LinkedList<>();
	List<Product> products = new LinkedList<>();
	List<Review> reviews = new LinkedList<>();

	// R1
	/**
	 * Define the activities types treated in the portal. The method can be invoked
	 * multiple times to add different activities.
	 * 
	 * @param actvities
	 *            names of the activities
	 * @throws SportsException
	 *             thrown if no activity is provided
	 */
	public void defineActivities(String... activities) throws SportsException {
		if (activities.length == 0) {
			throw new SportsException("No activity");
		}
		for (String activity : activities) {
			this.activities.add(activity);
		}
	}

	/**
	 * Retrieves the names of the defined activities.
	 * 
	 * @return activities names sorted alphabetically
	 */
	public List<String> getActivities() {
		return this.activities.stream().sorted((a, b) -> a.compareTo(b)).collect(Collectors.toList());
	}

	/**
	 * Add a new category of sport products and the linked activities
	 * 
	 * @param name
	 *            name of the new category
	 * @param activities
	 *            reference activities for the category
	 * @throws SportsException
	 *             thrown if any of the specified activity does not exist
	 */
	public void addCategory(String name, String... linkedActivities) throws SportsException {
		for (String act : linkedActivities) {
			if (!this.activities.contains(act)) {
				throw new SportsException("Missing activity");
			}
		}
		Category tmp = new Category(name);
		tmp.addActivities(linkedActivities);
		this.categories.add(tmp);
	}

	/**
	 * Retrieves number of categories.
	 * 
	 * @return categories count
	 */
	public int countCategories() {
		return this.categories.size();
	}

	/**
	 * Retrieves all the categories linked to a given activity.
	 * 
	 * @param activity
	 *            the activity of interest
	 * @return list of categories (sorted alphabetically)
	 */
	public List<String> getCategoriesForActivity(String activity) {
		if (!this.containActivity(activity)) {
			return new LinkedList<>();
		}
		String act = this.getActivity(activity);
		return this.categories.stream().filter(c -> c.containActivity(act)).map(Category::getName)
				.sorted(String::compareTo).collect(Collectors.toList());
	}

	private Category getCategory(String name) {
		Optional<Category> tmp = this.categories.parallelStream().filter(c -> c.getName().equals(name)).findFirst();
		if (!tmp.isPresent()) {
			return null;
		}
		return tmp.get();
	}

	private boolean containProduct(String pName) {
		return this.products.parallelStream().anyMatch(p -> p.sameName(pName));
	}

	private boolean containActivity(String name) {
		return this.activities.contains(name);
	}

	private String getActivity(String name) {
		Optional<String> tmp = this.activities.parallelStream().filter(c -> c.equals(name)).findFirst();
		if (!tmp.isPresent()) {
			return null;
		}
		return tmp.get();
	}

	private Product getProduct(String name) {
		Optional<Product> tmp = this.products.parallelStream().filter(c -> c.getName().equals(name)).findFirst();
		if (!tmp.isPresent()) {
			return null;
		}
		return tmp.get();
	}

	// R2
	/**
	 * Add a research group and the relative disciplines.
	 * 
	 * @param name
	 *            name of the research group
	 * @param disciplines
	 *            list of disciplines
	 * @throws SportsException
	 *             thrown in case of duplicate name
	 */
	public void addProduct(String name, String activityName, String categoryName) throws SportsException {
		if (this.containProduct(name)) {
			throw new SportsException("Product exist");
		}
		this.products.add(new Product(name, this.getActivity(activityName), this.getCategory(categoryName)));
	}

	/**
	 * Retrieves the list of products for a given category. The list is sorted
	 * alphabetically.
	 * 
	 * @param categoryName
	 *            name of the category
	 * @return list of products
	 */
	public List<String> getProductsForCategory(String categoryName) {
		Category tmp = this.getCategory(categoryName);
		if (tmp == null) {
			return new LinkedList<>();
		}
		return this.products.stream().filter(p -> p.getCategory().equals(tmp)).map(Product::getName)
				.sorted(String::compareTo).collect(Collectors.toList());
	}

	/**
	 * Retrieves the list of products for a given activity. The list is sorted
	 * alphabetically.
	 * 
	 * @param activityName
	 *            name of the activity
	 * @return list of products
	 */
	public List<String> getProductsForActivity(String activityName) {
		String activity = this.getActivity(activityName);
		if (activity == null) {
			return new LinkedList<>();
		}
		return this.products.stream().filter(p -> p.getActivity().equals(activityName)).map(Product::getName)
				.sorted(String::compareTo).collect(Collectors.toList());
	}

	/**
	 * Retrieves the list of products for a given activity and a set of categories
	 * The list is sorted alphabetically.
	 * 
	 * @param activityName
	 *            name of the activity
	 * @param categoryNames
	 *            names of the categories
	 * @return list of products
	 */
	public List<String> getProducts(String activityName, String... categoryNames) {
		String activity = this.getActivity(activityName);
		return this.products.stream().filter(p -> p.getActivity().equals(activity)).filter(p -> {
			for (String category : categoryNames) {
				if (p.getCategory().getName().equals(category)) {
					return true;
				}
			}
			return false;
		}).map(Product::getName).sorted(String::compareTo).collect(Collectors.toList());
	}

	// //R3
	/**
	 * Add a new product rating
	 * 
	 * @param productName
	 *            name of the product
	 * @param userName
	 *            name of the user submitting the rating
	 * @param numStars
	 *            score of the rating in stars
	 * @param comment
	 *            comment for the rating
	 * @throws SportsException
	 *             thrown numStars is not correct
	 */
	public void addRating(String productName, String userName, int numStars, String comment) throws SportsException {
		this.reviews.add(new Review(this.getProduct(productName), userName, numStars, comment));
	}

	/**
	 * Retrieves the ratings for the given product. The ratings are sorted by
	 * descending number of stars.
	 * 
	 * @param productName
	 *            name of the product
	 * @return list of ratings sorted by stars
	 */
	public List<String> getRatingsForProduct(String productName) {
		return this.reviews.stream().filter(r -> r.getProduct().getName().equals(productName)).sorted(Review::compareTo)
				.map(Review::toString).collect(Collectors.toList());
	}

	// R4
	/**
	 * Returns the average number of stars of the rating for the given product.
	 * 
	 * 
	 * @param productName
	 *            name of the product
	 * @return average rating
	 */
	public double getStarsOfProduct(String productName) {
		OptionalDouble avg = this.reviews.parallelStream().filter(r -> r.getProduct().getName().equals(productName))
				.mapToInt(Review::getStars).average();
		return avg.isPresent() ? avg.getAsDouble() : 0;
	}

	/**
	 * Computes the overall average stars of all ratings
	 * 
	 * @return average stars
	 */
	public double averageStars() {
		OptionalDouble avg = this.reviews.parallelStream().mapToInt(Review::getStars).average();
		return avg.isPresent() ? avg.getAsDouble() : 0;

	}

	private int countReviewsPerProductName(String pName) {
		return (int) this.reviews.parallelStream().filter(r -> r.getProduct().getName().equals(pName)).count();
	}

	private List<Product> productsPerActivity(String aName) {
		return this.products.parallelStream().filter(p -> p.getActivity().equals(aName)).collect(Collectors.toList());
	}

	private Entry<String, Double> averagePerActivity(String aName) {
		OptionalDouble avg = this.productsPerActivity(aName).parallelStream()
				.filter(p -> this.countReviewsPerProductName(p.getName()) > 0)
				.mapToDouble(p -> this.getStarsOfProduct(p.getName())).average();
		return avg.isPresent() ? new AbstractMap.SimpleEntry<>(aName, avg.getAsDouble())
				: new AbstractMap.SimpleEntry<>(aName, -1.0);
	}

	// R5 Statistiche
	/**
	 * For each activity return the average stars of the entered ratings.
	 * 
	 * Activity names are sorted alphabetically.
	 * 
	 * @return the map associating activity name to average stars
	 */
	public SortedMap<String, Double> starsPerActivity() {
		Map<String, Double> tmp = this.activities.parallelStream().map(this::averagePerActivity)
				.filter(e -> e.getValue() != -1.0).collect(Collectors.toMap(Entry::getKey, Entry::getValue));
		TreeMap<String, Double> result = new TreeMap<>();
		result.putAll(tmp);
		return result;
	}

	private List<String> productsNameWithCertainAverage(Double score) {
		return this.products.stream().map(Product::getName).filter(p -> this.getStarsOfProduct(p) == score).distinct()
				.sorted(String::compareTo).collect(Collectors.toList());
	}

	private List<Double> averages() {
		return this.products.stream().map(Product::getName).filter(p -> this.countReviewsPerProductName(p) > 0)
				.map(this::getStarsOfProduct).collect(Collectors.toList());
	}

	/**
	 * For each average star rating returns a list of the products that have such
	 * score.
	 * 
	 * Ratings are sorted in descending order.
	 * 
	 * @return the map linking the average stars to the list of products
	 */
	public SortedMap<Double, List<String>> getProductsPerStars() {
		SortedMap<Double, List<String>> tmp = new TreeMap<>((a, b) -> b.compareTo(a));
		this.averages().stream().forEach(a -> tmp.put(a, this.productsNameWithCertainAverage(a)));
		return tmp;
	}

}