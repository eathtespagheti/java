package sports;

public class Review {
	private Product product;
	private String user;
	private Integer stars;
	private String comment;

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public Integer getStars() {
		return stars;
	}

	public void setStars(Integer stars) {
		this.stars = stars;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Review(Product product, String user, Integer stars, String comment) throws SportsException {
		super();
		if (stars < 0 || stars > 5) {
			throw new SportsException("Rating error");
		}
		this.stars = stars;
		this.product = product;
		this.user = user;
		this.comment = comment;
	}

	@Override
	public String toString() {
		return this.getStars() + " : " + this.getComment();
	}

	public int compareTo(Review r) {
		return r.getStars().compareTo(this.getStars());
	}

}
