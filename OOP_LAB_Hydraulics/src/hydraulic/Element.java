package hydraulic;

/**
 * Represents the generic abstract element of an hydraulics system. It is the
 * base class for all elements.
 *
 * Any element can be connect to a downstream element using the method
 * {@link #connect(Element) connect()}.
 */
public abstract class Element {
	private String name;
	private Element output;
	protected StringBuilder builder;

	/**
	 * Constructor
	 * 
	 * @param name the name of the element
	 */
	public Element(String name) {
		this.name = name;
		this.builder = new StringBuilder()
	}

	/**
	 * getter method
	 * 
	 * @return the name of the element
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Connects this element to a given element. The given element will be connected
	 * downstream of this element
	 * 
	 * @param elem the element that will be placed downstream
	 */
	public void connect(Element elem) {
		this.output = elem;
	}

	/**
	 * Retrieves the element connected downstream of this
	 * 
	 * @return downstream element
	 */
	public Element getOutput() {
		return this.output;
	}
	
	protected int printSpace(int numberOfSpaces) {
		if (numberOfSpaces <= 0) {
			return 0;
		}
		String spaceString = "";
		for (int i = 0; i < numberOfSpaces; i++) {
			spaceString += " ";
		}
		System.out.print(spaceString);
		return numberOfSpaces;
	}
	
	protected String getSeparator() {
		return this.getOutput() == null ? "* " : "-> ";	
	}
	
	protected String printItem() {
		return "[" + this.getName() + "]" + this.getClass().getName() + " " + this.getSeparator();
	}
	
	protected void layout(int spaceBefore) {
		String out = this.printItem();
		Element next = this.getOutput();
		if (next != null) {
			next.layout(space);
		} else {
			// Print return
			System.out.println("");
		}
	}

	public abstract void simulate(SimulationObserver s, double prevFlow);

}
