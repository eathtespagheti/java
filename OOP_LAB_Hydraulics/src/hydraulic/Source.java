package hydraulic;

/**
 * Represents a source of water, i.e. the initial element for the simulation.
 *
 * The status of the source is defined through the method
 * {@link #setFlow(double) setFlow()}.
 */
public class Source extends Element {
	private double flow;

	public Source(String name) {
		super(name);
		this.flow = SimulationObserver.NO_FLOW;
	}

	/**
	 * defines the flow produced by the source
	 * 
	 * @param flow
	 */
	public void setFlow(double flow) {
		this.flow = flow;
	}

	/**
	 * get the flow value
	 * 
	 * @return flow
	 */
	public double getFlow() {
		return this.flow;
	}
	
	@Override
	public void simulate(SimulationObserver s, double prevFlow) {
		s.notifyFlow(Source.class.getName(), this.getName(), SimulationObserver.NO_FLOW, this.getFlow());
		Element out = this.getOutput();
		if (out != null) {
			out.simulate(s, this.getFlow() + prevFlow);
		}
	}
}
