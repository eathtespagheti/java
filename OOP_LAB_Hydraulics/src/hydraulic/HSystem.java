package hydraulic;

import java.util.ArrayList;
import java.util.List;

/**
 * Main class that act as a container of the elements for the simulation of an
 * hydraulics system
 * 
 */
public class HSystem {
	protected List<Element> elements = new ArrayList<>();

	/**
	 * Adds a new element to the system
	 * 
	 * @param elem
	 */
	public void addElement(Element elem) {
		this.elements.add(elem);
	}

	/**
	 * returns the element added so far to the system. If no element is present in
	 * the system an empty array (length==0) is returned.
	 * 
	 * @return an array of the elements added to the hydraulic system
	 */
	public Element[] getElements() {
		return this.elements.toArray(new Element[0]);
	}

	/**
	 * Prints the layout of the system starting at each Source
	 */
	public String layout() {
		String output = "";
		for (Element element : this.elements) {
			if (element instanceof Source) {
				output = element.layout(0);
			}
		}
		return output;
	}

	/**
	 * starts the simulation of the system
	 */
	public void simulate(SimulationObserver observer) {
		for (Element element : this.elements) {
			if (element instanceof Source) {
				element.simulate(observer, 0);
			}
		}
	}

}
