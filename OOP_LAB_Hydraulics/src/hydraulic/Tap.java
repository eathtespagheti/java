package hydraulic;

/**
 * Represents a tap that can interrupt the flow.
 * 
 * The status of the tap is defined by the method {@link #setOpen(boolean)
 * setOpen()}.
 */

public class Tap extends Element {
	private boolean open;

	public Tap(String name) {
		super(name);
		this.open = false;
	}

	/**
	 * Defines whether the tap is open or closed.
	 * 
	 * @param open opening level
	 */
	public void setOpen(boolean open) {
		this.open = open;
	}

	/**
	 * Get open value
	 * 
	 * @return open
	 */
	public boolean getOpen() {
		return this.open;
	}

	private double getFlow(double prevFlow) {
		return this.open ? prevFlow : 0;
	}

	@Override
	public void simulate(SimulationObserver s, double prevFlow) {
		double outFlow = this.getFlow(prevFlow);
		s.notifyFlow(Tap.class.getName(), this.getName(), prevFlow, outFlow);
		Element out = this.getOutput();
		if (out != null) {
			out.simulate(s, outFlow);
		}
	}

}
