package hydraulic;

/**
 * Represents the sink, i.e. the terminal element of a system
 *
 */
public class Sink extends Element {

	/**
	 * Constructor
	 * 
	 * @param name
	 */
	public Sink(String name) {
		super(name);
	}

	/**
	 * Connects this element to a given element. The given element will be connected
	 * downstream of this element
	 * 
	 * @param elem the element that will be placed downstream
	 */
	@Override
	public void connect(Element elem) {
	}

	/**
	 * Retrieves the element connected downstream of this
	 * 
	 * @return downstream element
	 */
	@Override
	public Element getOutput() {
		return this;
	}

	@Override
	public void simulate(SimulationObserver s, double prevFlow) {
		s.notifyFlow(Sink.class.getName(), this.getName(), prevFlow, SimulationObserver.NO_FLOW);
	}

	@Override
	protected String getSeparator() {
		return "*";
	}

	@Override
	protected void layout(int spaceBefore) {
		this.printItem(spaceBefore);
		System.out.println("");
	}

}
