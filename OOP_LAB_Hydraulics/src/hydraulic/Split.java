package hydraulic;

/**
 * Represents a split element, a.k.a. T element
 * 
 * During the simulation each downstream element will receive a stream that is
 * half the input stream of the split.
 */

public class Split extends Element {
	protected Element[] outputs;
	protected double[] proportions;

	/**
	 * Constructor
	 * 
	 * @param name
	 */
	public Split(String name) {
		super(name);
		this.initialize(2);
	}

	protected Split(String name, int numberOfElements) {
		super(name);
		this.initialize(numberOfElements);
	}

	protected void initialize(int numberOfElements) {
		this.outputs = new Element[numberOfElements];
		this.proportions = new double[numberOfElements];
		double splitDefault = 1.0 / this.outputs.length;
		for (int i = 0; i < this.proportions.length; i++) {
			this.proportions[i] = splitDefault;
		}
	}

	/**
	 * returns the downstream elements
	 * 
	 * @return array containing the two downstream element
	 */
	public Element[] getOutputs() {
		return this.outputs;
	}

	/**
	 * connect one of the outputs of this split to a downstream component.
	 * 
	 * @param elem    the element to be connected downstream
	 * @param noutput the output number to be used to connect the element
	 */
	public void connect(Element elem, int noutput) {
		if (noutput >= this.outputs.length && noutput < 0) {
			return;
		}
		this.outputs[noutput] = elem;
	}

	@Override
	public void simulate(SimulationObserver s, double prevFlow) {
		s.notifyFlow(Split.class.getName(), this.getName(), prevFlow, this.proportions);
		for (int i = 0; i < this.outputs.length; i++) {
			if (this.outputs[i] != null) {
				this.outputs[i].simulate(s, this.proportions[i]);
			}
		}
	}

	@Override
	protected String getSeparator() {
		return this.getOutputs() == null ? "* " : "+ ";
	}

	protected void printSeparator(int spaceBefore) {
		spaceBefore -= 2;
		this.printSpace(spaceBefore);
		System.out.println("|");
		this.printSpace(spaceBefore);
		System.out.print(this.getSeparator());
	}

	@Override
	protected void layout(int spaceBefore) {
		int space = this.printItem(spaceBefore);
		boolean separator = false;
		for (Element element : outputs) {
			if (element != null) {
				if (separator) {
					this.printSeparator(space);
				}
				element.layout(space);
				separator = true;
			}
		}
		if (!separator) {
			// Print return
			System.out.println("");
		}
	}

}
