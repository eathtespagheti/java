# Simulazione Idraulica

## Realizzare il sistema software per la descrizione e la simulazione di un sistema idraulico. Tutte le classi si trovano nel package hydraulic

### R1: Elementi e Tubi

Un sistema idraulico è composto da elementi di vario tipo connessi tra loro (tramite tubi che però non sono modellati esplicitamente con questo software).

Un sistema idraulico è rappresentato da un oggetto di classe HSystem; questa classe permette di aggiungere nuovi elementi tramite il metodo addElement(), il quale riceve come parametro un oggetto Element e lo aggiunge ai componenti che formano il sistema idraulico.

Tramite il metodo getElements() è possibile ottenere un array contenente tutti e soli gli elementi presenti nel sistema, questo metodo restituisce un array di oggetti Element.

Tutti gli elementi hanno un nome che può essere letto tramite il metodo getName().

### R2: Elementi semplici

Sono definiti tre tipi di elementi semplici: sorgente, rubinetto e scarico, che sono rispettivamente rappresentati dalle classi Source, Tap e Sink.

È possibile connettere l'uscita di un elemento all'ingresso di un altro tramite il metodo connect(); il metodo riceve come parametro l'elemento al cui ingresso deve essere connessa l'uscita dell'elemento sui cui è invocato: ad esempio, a.connect(b); connette l'uscita di a all'ingresso di b. Il metodo connect() se invocato su un oggetto Sink non ha nessun effetto.

Dato un elemento semplice qualunque, è possibile sapere a quale altro elemento è connessa la sua uscita, tramite il metodo getOutput() che resituisce un oggetto di tipo Element.

### R3: Elementi complessi

Oltre agli elementi semplici, sopra descritti,è possibile utilizzare degli elementi complessi. L'elemento a T, rappresentato dalla classe Split, permette di suddividere l'ingresso in due flussi in uscita uguali tra loro. Per tale classe il metodo connect() riceve un ulteriore parametro, di tipo intero, che indica il numero dell'uscita a cui connettere l'elemento. Tale intero ha valore 0 per la prima uscita e 1 per la seconda.

Per sapere quali elementi sono connessi in uscita ad un elemento a T, è possibile utilizzare il metodo getOutputs() che restituisce un array con i due elementi connessi.

### R4: Simulazione

Dato un sistema corretto, ovvero un albero che ha come radice una sorgente ed in cui ogni percorso termina con uno scarico, è possibile fare un calcolo delle portate e di come vengono ripartite nei vari elementi.

Il calcolo prevede due fasi: una prima fase (setup) i cui si definiscono i parametri dei diversi elementi del sistema e una seconda fase (simulazione) in cui si avvia la simulazione.

Durante la fase si setup è possibile:

definire la portata per una sorgente (Source) con il metodo setFlow(), che riceve come parametro un numero reale che rappresenta i metri cubi al secondo erogati dalla sorgente
impostare l'apertura dei rubinetti (Tap), tramite il metodo setOpen(), che riceve come parametro un boolean. Se un rubinetto è aperto la portata in uscita è uguale a quell in ingresso, altrimenti la portata in uscita è nulla (0.0).
Per i raccordi a T la portata in ingresso viene ripartita equamente tra le due uscite.

Il metodo simulate() della classe HSystem, effettua i calcoli di portata a partire da ogni sorgente e notifica, per ogni elemento: il nome e le portate in ingresso e in uscita. Questo metodo richiede come parametro un oggetto che implementa l'interfaccia SimulationObserver, che presenta un unico metodo.

Quando, durante la simulazione, sono noti i flussi in entrata ed in uscita per un elemento deve essere invocato il metodo notifyFlow() passando il tipo di elemento (nome della classe), il nome dell'elemento, ed i flussi in ingresso ed uscita; se uno dei flussi non è definito (ad es. per Source e Sink) si usa la costante NO_FLOW.

Suggerimento: dato un oggetto, per sapere se è un'istanza di una classe si può usare l'operatore instanceof. Es. if(element instanceof Source)
Attenzione: non è richiesto implementare l'interfaccia SimulationObserver ma solamente usarla; per scopi di verifica viene fornito un esempio di implementazione (classe PrintingObserver) che semplicemente stampa su console le notifiche.
Version 2.0 - 2020-04-16
