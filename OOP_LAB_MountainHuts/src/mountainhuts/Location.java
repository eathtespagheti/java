/**
 * 
 */
package mountainhuts;

/**
 * @author fabio
 *
 */
public abstract class Location {
	private String name;
	protected Integer altitude;

	protected Location() {
		super();
	}

	/**
	 * @param name
	 */
	public Location(String name, Integer altitude) {
		super();
		this.setName(name);
		this.setAltitude(altitude);
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param altitude the altitude to set
	 */
	public void setAltitude(Integer altitude) {
		this.altitude = altitude;
	}

	abstract <T> T getAltitude();

	/**
	 * @return create a new instance of the class
	 */
	abstract void parametersConstructor(String name, Integer altitude, Object... parameters);
}
