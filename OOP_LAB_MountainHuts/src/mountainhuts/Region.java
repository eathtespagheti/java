package mountainhuts;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

/**
 * Class {@code Region} represents the main facade class for the mountains hut
 * system.
 * 
 * It allows defining and retrieving information about municipalities and
 * mountain huts.
 *
 */
/**
 * @author fabio
 *
 */
public class Region {
	private String name;
	private HashMap<Integer, Integer> ranges;
	private ArrayList<Municipality> municipalities;
	private ArrayList<MountainHut> mountainhuts;

	/**
	 * Create a region with the given name.
	 * 
	 * @param name the name of the region
	 */
	public Region(String name) {
		this.name = name;
		this.ranges = new HashMap<>();
	}

	/**
	 * Return the name of the region.
	 * 
	 * @return the name of the region
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Create the ranges given their textual representation in the format
	 * "[minValue]-[maxValue]".
	 * 
	 * @param ranges an array of textual ranges
	 */
	public void setAltitudeRanges(String... ranges) {
		Stream.of(ranges).forEach(this::addAltitudeRange);
	}

	/**
	 * Return the textual representation in the format "[minValue]-[maxValue]" of
	 * the range including the given altitude or return the default range "0-INF".
	 * 
	 * @param altitude the geographical altitude
	 * @return a string representing the range
	 */
	public String getAltitudeRange(Integer altitude) {
		Optional<Entry<Integer, Integer>> search = this.ranges.entrySet().parallelStream()
				.filter(e -> altitude <= e.getValue().intValue() && altitude >= e.getKey().intValue()).findFirst();
		if (search.isPresent()) {
			return search.get().getKey() + "-" + search.get().getValue();
		}
		return "0 - INF";
	}

	/**
	 * Return the numerical representation in the format of the range including the
	 * given altitude or return the default range 0 - INF(MaxInt).
	 * 
	 * @param altitude the geographical altitude
	 * @return a string representing the range
	 */
	public Entry<Integer, Integer> getAllAltitudeRanges(Integer altitude) {
		String toParse = this.getAltitudeRange(altitude);
		String[] splitted = toParse.split(" - ");
		Integer max = Integer.MAX_VALUE;
		if (!splitted[1].equals("INF")) {
			max = Integer.parseInt(splitted[1]);
		}
		return new AbstractMap.SimpleEntry<>(altitude, max);
	}

	/**
	 * Create a new municipality if it is not already available or find it.
	 * Duplicates must be detected by comparing the municipality names.
	 * 
	 * @param name     the municipality name
	 * @param province the municipality province
	 * @param altitude the municipality altitude
	 * @return the municipality
	 */
	public Municipality createOrGetMunicipality(String name, String province, Integer altitude) {
		return this.createOrGetLocation(this.getMunicipalities(), Municipality.class, name, altitude, province);
	}

	private <T extends Location> T createOrGetLocation(Collection<T> collection, Class<T> locationType, String name,
			Integer altitude, Object... parameters) {
		Optional<T> searchResult = collection.stream().parallel().filter(l -> l.getName().equals(name)).findFirst();
		if (!searchResult.isPresent()) {
			T tmp;
			try {
				tmp = locationType.newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
				return null;
			}
			tmp.parametersConstructor(name, altitude, parameters);
			searchResult = Optional.of(tmp);
			collection.add(searchResult.get());
		}
		return searchResult.get();
	}

	/**
	 * Return all the municipalities available.
	 * 
	 * @return a collection of municipalities
	 */
	public Collection<Municipality> getMunicipalities() {
		if (this.municipalities == null) {
			this.municipalities = new ArrayList<>();
		}
		return this.municipalities;
	}

	/**
	 * Create a new mountain hut if it is not already available or find it.
	 * Duplicates must be detected by comparing the mountain hut names.
	 *
	 * @param name         the mountain hut name
	 * @param category     the mountain hut category
	 * @param bedsNumber   the number of beds in the mountain hut
	 * @param municipality the municipality in which the mountain hut is located
	 * @return the mountain hut
	 */
	public MountainHut createOrGetMountainHut(String name, String category, Integer bedsNumber,
			Municipality municipality) {
		return this.createOrGetLocation(this.getMountainHuts(), MountainHut.class, name, null, category, bedsNumber,
				municipality);

	}

	/**
	 * Create a new mountain hut if it is not already available or find it.
	 * Duplicates must be detected by comparing the mountain hut names.
	 * 
	 * @param name         the mountain hut name
	 * @param altitude     the mountain hut altitude
	 * @param category     the mountain hut category
	 * @param bedsNumber   the number of beds in the mountain hut
	 * @param municipality the municipality in which the mountain hut is located
	 * @return a mountain hut
	 */
	public MountainHut createOrGetMountainHut(String name, Integer altitude, String category, Integer bedsNumber,
			Municipality municipality) {
		return this.createOrGetLocation(this.getMountainHuts(), MountainHut.class, name, altitude, category, bedsNumber,
				municipality);
	}

	/**
	 * Return all the mountain huts available.
	 * 
	 * @return a collection of mountain huts
	 */
	public Collection<MountainHut> getMountainHuts() {
		if (this.mountainhuts == null) {
			this.mountainhuts = new ArrayList<>();
		}
		return this.mountainhuts;
	}

	/**
	 * Factory methods that creates a new region by loadomg its data from a file.
	 * 
	 * The file must be a CSV file and it must contain the following fields:
	 * <ul>
	 * <li>{@code "Province"},
	 * <li>{@code "Municipality"},
	 * <li>{@code "MunicipalityAltitude"},
	 * <li>{@code "Name"},
	 * <li>{@code "Altitude"},
	 * <li>{@code "Category"},
	 * <li>{@code "BedsNumber"}
	 * </ul>
	 * 
	 * The fields are separated by a semicolon (';'). The field {@code "Altitude"}
	 * may be empty.
	 * 
	 * @param name the name of the region
	 * @param file the path of the file
	 */
	public static Region fromFile(String name, String file) {
		List<String> lines;
		try (BufferedReader in = new BufferedReader(new FileReader(file))) {
			lines = in.lines().collect(toList());
		} catch (IOException e) {
			Logger logger = Logger.getAnonymousLogger(Region.class.getName());
			logger.log(Level.SEVERE, e.getMessage());
			return null;
		}
		Region fromFile = new Region(name);
		lines.stream().skip(1).forEach(fromFile::addMunicipalityMountainHutFromString);
		return fromFile;
	}

	/**
	 * Count the number of municipalities with at least a mountain hut per each
	 * province.
	 * 
	 * @return a map with the province as key and the number of municipalities as
	 *         value
	 */
	public Map<String, Long> countMunicipalitiesPerProvince() {
		return this.getProvinces().parallelStream()
				.collect(toMap(String::toString, this::countMunicipalitiesPerProvince));
	}

	/**
	 * Count the number of mountain huts per each municipality within each province.
	 * 
	 * @return a map with the province as key and, as value, a map with the
	 *         municipality as key and the number of mountain huts as value
	 */
	public Map<String, Map<String, Long>> countMountainHutsPerMunicipalityPerProvince() {
		Map<String, Long> secondMap = this.countMountainHutsPerMunicipality();
		return this.getProvinces().parallelStream()
				.collect(toMap(String::toString,
						p -> secondMap.entrySet().parallelStream()
								.filter(m -> this.getMunicipalitiesPerProvince(p).contains(m.getKey()))
								.collect(toMap(Entry::getKey, Entry::getValue))));
	}

	/**
	 * Get a List with all the Municipalities inside a province
	 * 
	 * @param province
	 * @return List of all the municipalities
	 */
	private List<String> getMunicipalitiesPerProvince(String province) {
		return this.getMunicipalities().parallelStream().filter(m -> m.getProvince().equals(province))
				.map(Municipality::getName).collect(toList());
	}

	/**
	 * Count the number of mountain huts per altitude range. If the altitude of the
	 * mountain hut is not available, use the altitude of its municipality.
	 * 
	 * @return a map with the altitude range as key and the number of mountain huts
	 *         as value
	 */
	public Map<String, Long> countMountainHutsPerAltitudeRange() {
		return this.ranges.entrySet().parallelStream().collect(toMap(e -> this.getAltitudeRange(e.getKey()),
				e -> this.countMountainHutsPerAltitudeRange(e.getKey(), e.getValue())));
	}

	/**
	 * Count the number of mountain huts per municipality.
	 * 
	 * @return a map with the municipality name as key and the number of mountain
	 *         huts as value
	 */
	public Map<String, Long> countMountainHutsPerMunicipality() {
		return this.getMunicipalities().parallelStream()
				.collect(toMap(Municipality::getName, this::countMountainHutsPerMunicipality));
	}

	/**
	 * Compute the total number of beds available in the mountain huts per each
	 * province.
	 * 
	 * @return a map with the province as key and the total number of beds as value
	 */
	public Map<String, Integer> totalBedsNumberPerProvince() {
		return this.getProvinces().parallelStream().collect(toMap(String::toString, this::bedsForProvince));
	}

	/**
	 * Compute the maximum number of beds available in a single mountain hut per
	 * altitude range. If the altitude of the mountain hut is not available, use the
	 * altitude of its municipality.
	 * 
	 * @return a map with the altitude range as key and the maximum number of beds
	 *         as value
	 */
	public Map<String, Optional<Integer>> maximumBedsNumberPerAltitudeRange() {
		return this.ranges.entrySet().parallelStream().collect(toMap(r -> this.getAltitudeRange(r.getKey()),
				r -> Optional.ofNullable(this.maxBedsForAltitudeRange(r.getKey(), r.getValue()))));
	}

	/**
	 * Compute the municipality names per number of mountain huts in a municipality.
	 * The lists of municipality names must be in alphabetical order.
	 * 
	 * @return a map with the number of mountain huts in a municipality as key and a
	 *         list of municipality names as value
	 */
	public Map<Long, List<String>> municipalityNamesPerCountOfMountainHuts() {
		Map<String, Long> map = this.countMountainHutsPerMunicipality();
		List<Long> numbers = map.entrySet().parallelStream().map(Entry::getValue).distinct().collect(toList());
		return numbers.parallelStream().collect(toMap(o -> o, c -> map.entrySet().parallelStream()
				.filter(e -> e.getValue().equals(c)).map(Entry::getKey).sorted().collect(toList())));
	}

	private Entry<Integer, Integer> parseAltitudeRange(String range) {
		String[] splitted = range.split("-");
		return new AbstractMap.SimpleEntry<>(Integer.parseInt(splitted[0]), Integer.parseInt(splitted[1]));
	}

	private void addAltitudeRange(String s) {
		Entry<Integer, Integer> tmp = this.parseAltitudeRange(s);
		this.ranges.put(tmp.getKey(), tmp.getValue());
	}

	private void addMunicipalityMountainHutFromString(String parameters) {
		String[] splitted = parameters.split(";");
		Municipality tmp = this.createOrGetMunicipality(splitted[1], splitted[0], Integer.parseInt(splitted[2]));

		if (splitted[4].equals("")) {
			this.createOrGetMountainHut(splitted[3], splitted[5], Integer.parseInt(splitted[6]), tmp);
		} else {
			this.createOrGetMountainHut(splitted[3], Integer.parseInt(splitted[4]), splitted[5],
					Integer.parseInt(splitted[6]), tmp);
		}
	}

	/*
	 * Return an ArrayList with all the provinces' names
	 */
	private List<String> getProvinces() {
		return this.getMunicipalities().parallelStream().map(Municipality::getProvince).distinct().collect(toList());
	}

	/*
	 * Return number of municipalities per province
	 */
	private Long countMunicipalitiesPerProvince(String province) {
		return this.getMunicipalities().parallelStream().filter(m -> m.getProvince().equals(province)).count();
	}

	/**
	 * Return number of MountainHuts in a Municipality
	 * 
	 * @param municipality
	 * @return number of MountainHuts
	 */
	private Long countMountainHutsPerMunicipality(String municipality) {
		return this.getMountainHuts().parallelStream().filter(m -> m.getMunicipality().getName().equals(municipality))
				.count();
	}

	/**
	 * Return number of MountainHuts in a Municipality
	 * 
	 * @param municipality
	 * @return number of MountainHuts
	 */
	private Long countMountainHutsPerMunicipality(Municipality municipality) {
		return this.countMountainHutsPerMunicipality(municipality.getName());
	}

	/**
	 * Count the number of mountain huts for a certain altitude range
	 * 
	 * @param max
	 * @return number of mountain huts
	 */
	private Long countMountainHutsPerAltitudeRange(Integer min, Integer max) {
		return this.getMountainHuts().parallelStream().filter(m -> {
			int altitude = m.getAltitudeSafe().intValue();
			return altitude >= min && altitude <= max;
		}).count();
	}

	private Integer bedsForMunicipality(Municipality municipality) {
		return this.getMountainHuts().parallelStream().filter(m -> m.getMunicipality().equals(municipality)).reduce(0,
				(total, element) -> total + element.getBeds(), Integer::sum);
	}

	private Integer bedsForProvince(String province) {
		return this.getMunicipalities().parallelStream().filter(m -> m.getProvince().equals(province)).reduce(0,
				(total, municipality) -> total + this.bedsForMunicipality(municipality), Integer::sum);
	}

	private Integer maxBedsForAltitudeRange(Integer min, Integer max) {
		return this.getMountainHuts().parallelStream().filter(m -> {
			int altitude = m.getAltitudeSafe().intValue();
			return altitude >= min && altitude <= max;
		}).mapToInt(MountainHut::getBeds).max().getAsInt();
	}

}
