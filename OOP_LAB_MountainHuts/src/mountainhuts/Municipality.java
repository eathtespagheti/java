package mountainhuts;

/**
 * Represents a municipality
 *
 */
public class Municipality extends Location {
	private String province;

	/**
	 * 
	 */
	protected Municipality() {
		super();
	}

	/**
	 * @param name
	 * @param altitude
	 * @param province
	 */
	public Municipality(String name, Integer altitude, String province) {
		super(name, altitude);
		this.setProvince(province);
	}

	/**
	 * @param province the province to set
	 */
	public void setProvince(String province) {
		this.province = province;
	}

	/**
	 * Province of the municipality
	 * 
	 * @return province
	 */
	public String getProvince() {
		return this.province;
	}

	public Integer getAltitude() {
		return this.altitude;
	}

	@Override
	void parametersConstructor(String name, Integer altitude, Object... parameters) {
		this.setName(name);
		this.setAltitude(altitude);
		this.setProvince((String) parameters[0]);
	}

}
