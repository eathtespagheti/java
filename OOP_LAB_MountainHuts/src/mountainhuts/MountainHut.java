package mountainhuts;

import java.util.Optional;

/**
 * Represents a mountain hut.
 * 
 * It is linked to a {@link Municipality}
 *
 */
public class MountainHut extends Location {
	private String category;
	private Integer beds;
	private Municipality municipality;

	/**
	 * 
	 */
	protected MountainHut() {
		super();
	}

	/**
	 * @param name
	 * @param altitude
	 * @param category
	 * @param beds
	 * @param municipality
	 */
	public MountainHut(String name, Integer altitude, String category, Integer beds, Municipality municipality) {
		super(name, altitude);
		this.setCategory(category);
		this.setBeds(beds);
		this.setMunicipality(municipality);
	}

	/**
	 * Altitude of the mountain hut. May be absent, in this case an empty
	 * {@link java.util.Optional} is returned.
	 * 
	 * @return optional containing the altitude
	 */
	@SuppressWarnings("unchecked")
	public Optional<Integer> getAltitude() {
		return Optional.ofNullable(this.altitude);
	}

	/**
	 * Altitude of the mountain hut. May be absent, in this case the altitude of the
	 * Municipality it's returned
	 * 
	 * @return altitude
	 */
	public Integer getAltitudeSafe() {
		return this.altitude == null ? this.getMunicipality().getAltitude() : this.getAltitude().get();
	}

	/**
	 * Category of the hut
	 * 
	 * @return the category
	 */
	public String getCategory() {
		return this.category;
	}

	/**
	 * @return the beds
	 */
	public Integer getBeds() {
		return beds;
	}

	/**
	 * @param beds the beds to set
	 */
	public void setBeds(Integer beds) {
		this.beds = beds;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @param municipality the municipality to set
	 */
	public void setMunicipality(Municipality municipality) {
		this.municipality = municipality;
	}

	/**
	 * Number of beds places available in the mountain hut
	 * 
	 * @return number of beds
	 */
	public Integer getBedsNumber() {
		return this.beds;
	}

	/**
	 * Municipality where the hut is located
	 * 
	 * @return municipality
	 */
	public Municipality getMunicipality() {
		return this.municipality;
	}

	@Override
	void parametersConstructor(String name, Integer altitude, Object... parameters) {
		this.setName(name);
		this.setAltitude(altitude);
		this.setCategory((String) parameters[0]);
		this.setBeds((Integer) parameters[1]);
		this.setMunicipality((Municipality) parameters[2]);
	}

}
