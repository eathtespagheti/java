package timetable;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class Path {
	private String code;
	private String category;
	private Boolean extraordinary;
	private LinkedList<TrainStop> stops = new LinkedList<>();
	private Timetable reference;

	/**
	 * @return the reference
	 */
	protected Timetable getReference() {
		return reference;
	}

	/**
	 * @param reference the reference to set
	 */
	protected void setReference(Timetable reference) {
		this.reference = reference;
	}

	/**
	 * @param Add a stop
	 */
	private void addStop(TrainStop s) {
		this.stops.add(s);
	}

	/**
	 * @param code
	 * @param category
	 */
	public Path(String code, String category) {
		super();
		this.setCode(code);
		this.setCategory(category);
		this.setExtraordinary(false);
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the extraordinary
	 */
	public Boolean isExtraordinary() {
		return this.extraordinary;
	}

	/**
	 * @param extraordinary the extraordinary to set
	 */
	public void setExtraordinary(Boolean extraordinary) {
		this.extraordinary = extraordinary;
	}

	public TrainStop addStop(String stationName, int hour, int minutes) {
		TrainStop tmp = new TrainStop(stationName, hour, minutes);
		this.addStop(tmp);
		return tmp;
	}

	public List<TrainStop> getTrainStops() {
		return this.stops;
	}

	protected Optional<TrainStop> getStop(String station) {
		return this.getTrainStops().parallelStream().filter(t -> t.getStation().equals(station)).findAny();
	}

	private Stream<Train> trainDelays() {
		return this.getReference().trainsOnPath(this).parallelStream();
	}

	public int maxDelay() {
		return this.trainDelays().mapToInt(Train::maxDelay).max().getAsInt();
	}

	public int minDelay() {
		return this.trainDelays().mapToInt(Train::minDelay).min().getAsInt();
	}

	public int totalDelay() {
		return this.trainDelays().mapToInt(Train::totalDelay).sum();
	}

	protected TrainStop getLastStop() {
		return this.getTrainStops().get(this.getTrainStops().size() - 1);
	}

}
