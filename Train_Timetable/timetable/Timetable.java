package timetable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Timetable {
	private ArrayList<Path> paths = new ArrayList<>();
	private LinkedList<Train> trains = new LinkedList<>();

	public Path createPath(String code, String category) {
		Path tmp = new Path(code, category);
		this.addPath(tmp);
		return tmp;
	}

	public Collection<Path> getPaths() {
		return this.paths;
	}

	public Path getPath(String code) {
		Optional<Path> search = this.getPaths().parallelStream().filter(p -> p.getCode().equals(code)).findAny();
		return search.isPresent() ? search.get() : null;
	}

	private void addTrain(Train t) {
		this.trains.add(t);
	}

	public Train newTrain(String code, int day, int month, int year) throws InvalidPath {
		Path tmp = this.getPath(code);
		if (tmp == null) {
			throw new InvalidPath();
		}
		Train t = new Train(tmp, day, month, year);
		this.addTrain(t);
		return t;
	}

	public List<Train> getTrains() {
		return this.trains;
	}

	private void addPath(Path p) {
		this.paths.add(p);
		p.setReference(this);
	}

	protected List<Train> trainsOnPath(Path p) {
		return this.getTrains().parallelStream().filter(t -> t.getPath().equals(p)).collect(Collectors.toList());
	}

}
