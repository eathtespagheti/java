package timetable;

import java.util.LinkedList;
import java.util.Optional;
import java.util.stream.IntStream;

public class Train {
	private Path path;
	private int day;
	private int month;
	private int year;
	private LinkedList<Passage> passages = new LinkedList<>();

	/**
	 * @param path
	 * @param day
	 * @param month
	 * @param year
	 */
	public Train(Path path, int day, int month, int year) {
		super();
		this.setPath(path);
		this.setDay(day);
		this.setMonth(month);
		this.setYear(year);
	}

	/**
	 * @return the path
	 */
	public Path getPath() {
		return path;
	}

	/**
	 * @param path the path to set
	 */
	public void setPath(Path path) {
		this.path = path;
	}

	/**
	 * @return the day
	 */
	public int getDay() {
		return day;
	}

	/**
	 * @param day the day to set
	 */
	public void setDay(int day) {
		this.day = day;
	}

	/**
	 * @return the month
	 */
	public int getMonth() {
		return month;
	}

	/**
	 * @param month the month to set
	 */
	public void setMonth(int month) {
		this.month = month;
	}

	/**
	 * @return the year
	 */
	public int getYear() {
		return year;
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(int year) {
		this.year = year;
	}

	private void addPassage(Passage p) {
		this.passages.add(p);
	}

	public Passage registerPassage(String string, int i, int j) throws InvalidStop {
		Optional<TrainStop> stopCheck = this.path.getStop(string);
		if (!stopCheck.isPresent()) {
			throw new InvalidStop();
		}
		TrainStop stop = stopCheck.get();
		int delay = (i - stop.getHour()) * 60;
		delay += j - stop.getMinutes();
		Passage tmp = new Passage(string, i, j, delay);
		this.addPassage(tmp);
		return tmp;
	}

	protected boolean checkIfStopHasPassage(TrainStop s) {
		return this.passages.parallelStream().anyMatch(p -> p.getStation().equals(s.getStation()));
	}

	public boolean arrived() {
		return this.checkIfStopHasPassage(this.getPath().getLastStop());
	}

	private IntStream getDelays() {
		return this.passages.stream().mapToInt(Passage::delay);
	}

	public int maxDelay() {
		return this.getDelays().max().getAsInt();
	}

	public int minDelay() {
		return this.getDelays().min().getAsInt();
	}

	public int totalDelay() {
		return this.getDelays().sum();
	}

}
