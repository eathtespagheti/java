package timetable;

public class TrainStop {
	private String station;
	private int hour;
	private int minutes;

	/**
	 * @param station
	 * @param hour
	 * @param minutes
	 */
	public TrainStop(String station, int hour, int minutes) {
		super();
		this.setStation(station);
		this.setHour(hour);
		this.setMinutes(minutes);
	}

	/**
	 * @return the station
	 */
	public String getStation() {
		return station;
	}

	/**
	 * @param station the station to set
	 */
	public void setStation(String station) {
		this.station = station;
	}

	/**
	 * @return the hour
	 */
	public int getHour() {
		return hour;
	}

	/**
	 * @param hour the hour to set
	 */
	public void setHour(int hour) {
		this.hour = hour;
	}

	/**
	 * @return the minutes
	 */
	public int getMinutes() {
		return minutes;
	}

	/**
	 * @param minutes the minutes to set
	 */
	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}

}
