package timetable;

public class Passage {
	private String station;
	private int hour;
	private int minutes;
	private int delay;

	/**
	 * @param station
	 * @param hour
	 * @param minutes
	 * @param delay
	 */
	public Passage(String station, int hour, int minutes, int delay) {
		super();
		this.setStation(station);
		this.setHour(hour);
		this.setMinutes(minutes);
		this.setDelay(delay);
	}

	/**
	 * @return the station
	 */
	public String getStation() {
		return station;
	}

	/**
	 * @return the hour
	 */
	public int getHour() {
		return hour;
	}

	/**
	 * @return the minutes
	 */
	public int getMinutes() {
		return minutes;
	}

	/**
	 * @return the delay
	 */
	public int delay() {
		return delay;
	}

	/**
	 * @param station the station to set
	 */
	private void setStation(String station) {
		this.station = station;
	}

	/**
	 * @param hour the hour to set
	 */
	private void setHour(int hour) {
		this.hour = hour;
	}

	/**
	 * @param minutes the minutes to set
	 */
	private void setMinutes(int minutes) {
		this.minutes = minutes;
	}

	/**
	 * @param delay the delay to set
	 */
	private void setDelay(int delay) {
		this.delay = delay;
	}

}
