package it.polito.oop.animal;

public class Cat {
    private String Name;

    public Cat() {
        this.Name = "Verdura";
    }

    public Cat(String Name) {
        this.setName(Name);
    }

    public String getName() {
        return this.Name;
    }

    // private void privateBark() {
    //     System.out.println(this.Name + " is barking privately!");
    // }

    public void meaow() {
        System.out.println(this.Name + " is meaowling!");
    }

    public void setName(String Name) {
        this.Name = Name;
    }
}
