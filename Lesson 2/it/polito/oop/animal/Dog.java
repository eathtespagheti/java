package it.polito.oop.animal;

public class Dog {
    private String Name;
    public Collar collar;

    public Dog() {
        this.Name = "Donald";
    }

    public Dog(String Name) {
        this.setName(Name);
    }

    public String getName() {
        return this.Name;
    }

    // private void privateBark() {
    // System.out.println(this.Name + " is barking privately!");
    // }

    public void bark() {
        System.out.println(this.Name + " is barking!");
    }

    public void setName(String Name) {
        this.Name = Name;
    }
}
