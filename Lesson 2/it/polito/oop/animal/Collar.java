package it.polito.oop.animal;

public class Collar {
    private String Color;

    public Collar(String Color) {
        setColor(Color);
    }

    public String getColor() {
        return this.Color;
    }

    public void setColor(String Color) {
        this.Color = Color;
    }
}
