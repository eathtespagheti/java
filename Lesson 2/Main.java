import it.polito.oop.animal.*;

public class Main {
    public static void main(String[] args) {
        Dog mainDog = new Dog();
        Dog secondDog = new Dog("Hitler");

        mainDog.bark();
        secondDog.bark();

        mainDog.setName("Adolf");
        mainDog.bark();
        secondDog.bark();
    }
}
