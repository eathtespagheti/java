/**
 * 
 */
package clinic;

/**
 * @author Fabio Sussarellu s251489
 * @email fabio.sussarellu@studenti.polito.it
 *
 */
public class Patient extends Person {

	/**
	 * @param name
	 * @param surname
	 * @param ssn
	 */
	public Patient(String name, String surname, String ssn) {
		super(name, surname, ssn);
	}

}
