/**
 * 
 */
package clinic;

/**
 * @author Fabio Sussarellu s251489
 * @email fabio.sussarellu@studenti.polito.it
 *
 */
public class Doctor extends Person {
	private int docID;
	private String specialization;

	/**
	 * @param name
	 * @param surname
	 * @param ssn
	 * @param docID
	 * @param specialization
	 */
	public Doctor(String name, String surname, String ssn, int docID, String specialization) {
		super(name, surname, ssn);
		this.setDocID(docID);
		this.setSpecialization(specialization);
	}

	/**
	 * @return the docID
	 */
	public int getDocID() {
		return docID;
	}

	/**
	 * @param docID the docID to set
	 */
	public void setDocID(int docID) {
		this.docID = docID;
	}

	/**
	 * @return the specialization
	 */
	public String getSpecialization() {
		return specialization;
	}

	/**
	 * @param specialization the specialization to set
	 */
	public void setSpecialization(String specialization) {
		this.specialization = specialization;
	}

	@Override
	public String toString() {
		return super.toString() + " [" + this.getDocID() + "]: " + this.getSpecialization();
	}

	@Override
	public Object getID() {
		return this.getDocID();
	}

}
