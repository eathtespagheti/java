package clinic;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

/**
 * Represents a clinic with patients and doctors.
 * 
 */
public class Clinic {
	private ArrayList<Patient> patients = new ArrayList<>();
	private ArrayList<Doctor> doctors = new ArrayList<>();
	private HashMap<Doctor, List<Patient>> assignations = new HashMap<>();

	/**
	 * Add a new clinic patient.
	 * 
	 * @param first first name of the patient
	 * @param last  last name of the patient
	 * @param ssn   SSN number of the patient
	 */
	public void addPatient(String first, String last, String ssn) {
		this.addPatient(new Patient(first, last, ssn));
	}

	/**
	 * Retrieves a patient information
	 * 
	 * @param ssn SSN of the patient
	 * @return the object representing the patient
	 * @throws NoSuchPatient in case of no patient with matching SSN
	 */
	public String getPatient(String ssn) throws NoSuchPatient {
		return this.getPatientObject(ssn).toString();
	}

	/**
	 * Add a new doctor working at the clinic
	 * 
	 * @param first          first name of the doctor
	 * @param last           last name of the doctor
	 * @param ssn            SSN number of the doctor
	 * @param docID          unique ID of the doctor
	 * @param specialization doctor's specialization
	 */
	public void addDoctor(String first, String last, String ssn, int docID, String specialization) {
		this.addDoctor(new Doctor(first, last, ssn, docID, specialization));
	}

	/**
	 * Retrieves information about a doctor
	 * 
	 * @param docID ID of the doctor
	 * @return object with information about the doctor
	 * @throws NoSuchDoctor in case no doctor exists with a matching ID
	 */
	public String getDoctor(int docID) throws NoSuchDoctor {
		return this.getDoctorObject(docID).toString();
	}

	/**
	 * Assign a given doctor to a patient
	 * 
	 * @param ssn   SSN of the patient
	 * @param docID ID of the doctor
	 * @throws NoSuchPatient in case of not patient with matching SSN
	 * @throws NoSuchDoctor  in case no doctor exists with a matching ID
	 */
	public void assignPatientToDoctor(String ssn, int docID) throws NoSuchPatient, NoSuchDoctor {
		Optional<Entry<Doctor, List<Patient>>> search = this.assignations.entrySet().parallelStream()
				.filter(a -> a.getKey().getID().equals(docID)).findFirst();
		if (search.isPresent()) {
			List<Patient> lista = search.get().getValue();
			Optional<Patient> paziente = lista.parallelStream().filter(p -> p.getID().equals(ssn)).findFirst();
			if (paziente.isPresent()) {
				return;
			}
			lista.add(getPatientObject(ssn));
		} else {
			this.assignations.put(this.getDoctorObject(docID), new LinkedList<Patient>());
			assignPatientToDoctor(ssn, docID);
		}
	}

	/**
	 * Retrieves the id of the doctor assigned to a given patient.
	 * 
	 * @param ssn SSN of the patient
	 * @return id of the doctor
	 * @throws NoSuchPatient in case of not patient with matching SSN
	 * @throws NoSuchDoctor  in case no doctor has been assigned to the patient
	 */
	public int getAssignedDoctor(String ssn) throws NoSuchPatient, NoSuchDoctor {
		Optional<Entry<Doctor, List<Patient>>> searchResult = this.assignations.entrySet().parallelStream()
				.filter(a -> a.getValue().parallelStream().anyMatch(p -> p.getID().equals(ssn))).findFirst();
		if (!searchResult.isPresent()) {
			this.getPatientObject(ssn); // Throw NoSuchPatient if patient doesn't exist
			throw new NoSuchDoctor();
		}
		return searchResult.get().getKey().getDocID();
	}

	/**
	 * Retrieves the patients assigned to a doctor
	 * 
	 * @param id ID of the doctor
	 * @return collection of patient SSNs
	 * @throws NoSuchDoctor in case the {@code id} does not match any doctor
	 */
	public Collection<String> getAssignedPatients(int id) throws NoSuchDoctor {
		Doctor d = this.getDoctorObject(id);
		return this.assignations.get(d).parallelStream().map(Patient::getSsn).collect(Collectors.toList());
	}

	/**
	 * Loads data about doctors and patients from the given stream.
	 * <p>
	 * The text file is organized by rows, each row contains info about either a
	 * patient or a doctor.
	 * </p>
	 * <p>
	 * Rows containing a patient's info begin with letter {@code "P"} followed by
	 * first name, last name, and SSN. Rows containing doctor's info start with
	 * letter {@code "M"}, followed by badge ID, first name, last name, SSN, and
	 * specialization.<br>
	 * The elements on a line are separated by the {@code ';'} character possibly
	 * surrounded by spaces that should be ignored.
	 * </p>
	 * <p>
	 * In case of error in the data present on a given row, the method should be
	 * able to ignore the row and skip to the next one.<br>
	 * 
	 * 
	 * @param readed linked to the file to be read
	 * @throws IOException in case of IO error
	 */
	public void loadData(Reader reader) throws IOException {
		List<Integer> line = new ArrayList<>();
		char separator = ';';

		int read = reader.read();
		while (read != -1) {
			if ((char) read == separator) {
				this.addPersonFromString(this.convertCollectionOfIntToString(line));
				line.clear();
			}
			if ((char) read != '\n') {
				line.add(read);
			}
		}
	}

	/**
	 * Generate a String from a Integer collection where every int represent a char
	 * 
	 * @param Integer collection
	 * @return String
	 */
	private String convertCollectionOfIntToString(Collection<Integer> c) {
		return new String(c.parallelStream().mapToInt(Integer::intValue).toArray(), 0,
				(int) c.parallelStream().count());
	}

	/**
	 * Parse a person from string and add it to the Clinic
	 * 
	 * @param String defining a person
	 */
	private void addPersonFromString(String s) {
		String[] splitted = s.split(" ");
		Optional<Person> result = this.parsePersonFromString(splitted);
		if (splitted[0].equals("P")) {
			if (result.isPresent()) {
				this.addPatient((Patient) result.get());
			}
		} else if (splitted[0].equals("M") && result.isPresent()) {
			this.addDoctor((Doctor) result.get());
		}
	}

	/**
	 * Parse a Doctor/Patient from a string
	 * 
	 * @param String
	 * @return Optional of Person
	 */
	private Optional<Person> parsePersonFromString(String... s) {
		Person result = null;
		if (s.length == 4) {
			result = this.parsePatientFromString(s);
		} else if (s.length == 6) {
			result = this.parseDoctorFromString(s);
		}
		return Optional.ofNullable(result);
	}

	/**
	 * Parse a Patient from an array of strings
	 * 
	 * @param Array of values as strings
	 * @return Doctor
	 */
	private Patient parsePatientFromString(String... s) {
		return new Patient(s[1], s[2], s[3]);
	}

	/**
	 * Parse a Doctor from an array of strings
	 * 
	 * @param Array of values as strings
	 * @return Doctor
	 */
	private Doctor parseDoctorFromString(String... s) {
		return new Doctor(s[1], s[2], s[3], Integer.parseInt(s[4]), s[5]);
	}

	/**
	 * Retrieves the collection of doctors that have no patient at all. The doctors
	 * are returned sorted in alphabetical order
	 * 
	 * @return the collection of doctors' ids
	 */
	public Collection<Integer> idleDoctors() {
		Collection<Doctor> filtered = this.doctors.parallelStream()
				.filter(d -> this.assignations.keySet().parallelStream().noneMatch(d2 -> d2.getID().equals(d.getID())))
				.collect(Collectors.toList());
		return filtered.stream().sorted((a, b) -> {
			int check = a.getSurname().compareTo(b.getSurname());
			return check != 0 ? check : a.getName().compareTo(b.getName());
		}).map(Doctor::getDocID).collect(Collectors.toList());
	}

	/**
	 * Retrieves the collection of doctors having a number of patients larger than
	 * the average.
	 * 
	 * @return the collection of doctors' ids
	 */
	public Collection<Integer> busyDoctors() {
		OptionalDouble average = this.assignations.values().parallelStream().mapToInt(Collection::size).average();
		if (!average.isPresent()) {
			return new ArrayList<>();
		}
		return this.assignations.entrySet().parallelStream().filter(e -> e.getValue().size() > average.getAsDouble())
				.map(e -> e.getKey().getDocID()).collect(Collectors.toList());
	}

	/**
	 * Retrieves the information about doctors and relative number of assigned
	 * patients.
	 * <p>
	 * The method returns list of strings formatted as
	 * "{@code ### : ID SURNAME NAME}" where {@code ###} represent the number of
	 * patients (printed on three characters).
	 * <p>
	 * The list is sorted by decreasing number of patients.
	 * 
	 * @return the collection of strings with information about doctors and patients
	 *         count
	 */
	public Collection<String> doctorsByNumPatients() {
		Map<Doctor, Integer> mapdata = this.assignations.entrySet().parallelStream()
				.collect(Collectors.toMap(Entry::getKey, e -> e.getValue().size()));
		return mapdata.entrySet().stream().sorted((a, b) -> b.getValue().compareTo(a.getValue()))
				.map(e -> "{" + String.format("%3d", e.getValue()) + " : " + e.getKey().getDocID() + " "
						+ e.getKey().getSurname() + " " + e.getKey().getName() + "}")
				.collect(Collectors.toList());
	}

	/**
	 * Retrieves the number of patients per (their doctor's) speciality
	 * <p>
	 * The information is a collections of strings structured as
	 * {@code ### - SPECIALITY} where {@code SPECIALITY} is the name of the
	 * speciality and {@code ###} is the number of patients cured by doctors with
	 * such speciality (printed on three characters).
	 * <p>
	 * The elements are sorted first by decreasing count and then by alphabetic
	 * speciality.
	 * 
	 * @return the collection of strings with speciality and patient count
	 *         information.
	 */
	public Collection<String> countPatientsPerSpecialization() {
		Map<String, Integer> counts = this.getActiveSpecializations().parallelStream()
				.collect(Collectors.toMap(String::toString, this::countPatientsAssignedToDoctorsWithSpecialization));
		return counts.entrySet().stream().sorted((a, b) -> {
			int check = a.getValue().compareTo(b.getValue());
			return check != 0 ? check : a.getKey().compareTo(b.getKey());
		}).map(e -> "{" + String.format("%3d", e.getValue()) + " - " + e.getKey() + "}").collect(Collectors.toList());
	}

	/**
	 * Return all the specializations from the doctors with at least one patient
	 * 
	 * @return Specializations types
	 */
	private Collection<String> getActiveSpecializations() {
		return this.assignations.keySet().parallelStream().map(Doctor::getSpecialization).distinct()
				.collect(Collectors.toList());
	}

	/**
	 * Count the number of Patients assigned to doctors with a certain
	 * specialization
	 * 
	 * @param specialization
	 * @return Number of patients
	 */
	private int countPatientsAssignedToDoctorsWithSpecialization(String specialization) {
		return this.assignations.entrySet().parallelStream()
				.filter(e -> e.getKey().getSpecialization().equals(specialization)).mapToInt(e -> e.getValue().size())
				.sum();
	}

	/**
	 * Add a person to a person collection
	 * 
	 * @param <P>        Person subtype
	 * @param person     object
	 * @param collection of persons
	 */
	private <P extends Person> void addPerson(P p, Collection<P> c) {
		c.add(p);
	}

	/**
	 * Get a person from a person collection
	 * 
	 * @param <P>        person subtype
	 * @param person     collection
	 * @param identifier for person object
	 * @return Optional person
	 */
	private <P extends Person> Optional<P> getPerson(Collection<P> c, Object id) {
		return c.parallelStream().filter(p -> p.getID().equals(id)).findFirst();
	}

	/**
	 * Get a patient from the patient ArrayList
	 * 
	 * @param ssn
	 * @return patient object
	 * @throws NoSuchPatient
	 */
	private Patient getPatientObject(String ssn) throws NoSuchPatient {
		Optional<Patient> search = this.getPerson(this.patients, ssn);
		if (!search.isPresent()) {
			throw new NoSuchPatient();
		}
		return search.get();
	}

	/**
	 * Get a doctor from the doctors ArrayList
	 * 
	 * @param docID
	 * @return doctor object
	 * @throws NoSuchDoctor
	 */
	private Doctor getDoctorObject(int docID) throws NoSuchDoctor {
		Optional<Doctor> search = this.getPerson(this.doctors, docID);
		if (!search.isPresent()) {
			throw new NoSuchDoctor();
		}
		return search.get();
	}

	/**
	 * Add a new clinic patient.
	 * 
	 * @param Patient
	 */
	private void addPatient(Patient p) {
		this.addPerson(p, this.patients);
	}

	/**
	 * Add a new clinic doctor.
	 * 
	 * @param Patient
	 */
	private void addDoctor(Doctor d) {
		this.addPerson(d, this.doctors);
	}
}
