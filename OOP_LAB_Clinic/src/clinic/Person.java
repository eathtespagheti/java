/**
 * 
 */
package clinic;

/**
 * @author Fabio Sussarellu s251489
 * @email fabio.sussarellu@studenti.polito.it
 *
 */
abstract class Person {
	private String name;
	private String surname;
	private String ssn;

	/**
	 * @param name
	 * @param surname
	 * @param ssn
	 */
	public Person(String name, String surname, String ssn) {
		super();
		this.setName(name);
		this.setSurname(surname);
		this.setSsn(ssn);
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * @param surname the surname to set
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}

	/**
	 * @return the ssn
	 */
	public String getSsn() {
		return ssn;
	}

	/**
	 * @param ssn the ssn to set
	 */
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	/**
	 * Return a unique identifier for the person
	 * 
	 * @return unique identifier
	 */
	public Object getID() {
		return this.getSsn();
	}

	@Override
	public String toString() {
		return this.getSurname() + " " + this.getName() + " (" + this.getSsn() + ")";
	}

}
