package university;

import java.util.HashMap;
import java.util.AbstractMap;
import java.util.Map;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * This class represents a university education system.
 * 
 * It manages students and courses.
 *
 */
public class University {
	static final int STUDENTS_ID_OFFSET = 10000;
	static final int COURSE_ID_OFFSET = 10;
	protected boolean logToFile = true;
	private String name;
	private Person rector;
	private StudentsList students = new StudentsList();
	private CourseList courses = new CourseList();
	private PrintWriter writer = new PrintWriter(new FileWriter("university_log.txt"), true);
	protected static final String STUDENT_BEGIN = "Student ";

	/**
	 * Generate the next student ID
	 * 
	 * @return next student ID
	 */
	private int getNextStudentID() {
		return this.students.getEnrolledStudents() + University.STUDENTS_ID_OFFSET;
	}

	/**
	 * Generate the next course ID
	 * 
	 * @return next student ID
	 */
	private int getNextCourseID() {
		return this.courses.size() + University.COURSE_ID_OFFSET;
	}

	/**
	 * Constructor
	 * 
	 * @param name name of the university
	 * @throws IOException
	 */
	public University(String name) throws IOException {
		this.name = name;
	}

	/**
	 * Getter for the name of the university
	 * 
	 * @return name of university
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Defines the rector for the university
	 * 
	 * @param first
	 * @param last
	 */
	public void setRector(String first, String last) {
		if (this.rector == null) {
			this.rector = new Person(first, last);
			return;
		}
		this.rector.setName(first);
		this.rector.setSurname(last);
	}

	/**
	 * Retrieves the rector of the university
	 * 
	 * @return
	 */
	public String getRector() {
		return this.rector.fullName();
	}

	/**
	 * Enroll a student in the university Return -1 if the maium number of students
	 * it's reached
	 * 
	 * @param first first name of the student
	 * @param last  last name of the student
	 * @return student id
	 */
	public int enroll(String first, String last) {
		int studentid = this.students.addStudent(this.getNextStudentID(), first, last);
		if (logToFile) {
			writer.println("New student enrolled: " + studentid + ", " + first + " " + last);
		}
		return studentid;
	}

	/**
	 * Retrieves the information for a given student
	 * 
	 * @param id the id of the student
	 * @return information about the student
	 */
	public String student(int id) {
		return this.students.findByID(id).toString();
	}

	/**
	 * Activates a new course with the given teacher
	 * 
	 * @param title   title of the course
	 * @param teacher name of the teacher
	 * @return the unique code assigned to the course
	 */
	public int activate(String title, String teacher) {
		int courseID = this.courses.addCourse(this.getNextCourseID(), title, teacher);
		if (logToFile) {
			writer.println("New course activated: " + courseID + " " + title + " " + teacher);
		}
		return courseID;
	}

	/**
	 * Retrieve the information for a given course
	 * 
	 * @param code unique code of the course
	 * @return information about the course
	 */
	public String course(int code) {
		Course c = this.courses.findByID(code);
		return c == null ? "null" : c.toString();
	}

	/**
	 * Register a student to attend a course
	 * 
	 * @param studentID  id of the student
	 * @param courseCode id of the course
	 */
	public void register(int studentID, int courseCode) {
		Course c = this.courses.findByID(courseCode);
		Student s = this.students.findByID(studentID);
		if (c == null || s == null) {
			return;
		}
		if (logToFile) {
			writer.println(University.STUDENT_BEGIN + s.getID() + " signed up for course " + c.getID());
		}
		c.addStudent(s);
	}

	/**
	 * Retrieve a list of attendees
	 * 
	 * @param courseCode unique id of the course
	 * @return list of attendees separated by "\n"
	 */
	public String listAttendees(int courseCode) {
		Course c = this.courses.findByID(courseCode);
		if (c == null) {
			return "null";
		}
		return c.studentsList();
	}

	/**
	 * Retrieves the study plan for a student
	 * 
	 * @param studentID id of the student
	 * @return list of courses the student is registered for
	 */
	public String studyPlan(int studentID) {
		Student s = this.students.findByID(studentID);
		if (s == null) {
			return "";
		}
		return this.courses.studyPlan(s);
	}

	/**
	 * Add a exam score
	 * 
	 * @param studentID
	 * @param courseID
	 * @param score
	 * @return true if successful, false if student doesn't exist or score it's
	 *         invalid
	 */
	public void exam(int studentID, int courseID, int score) {
		Student s = this.students.findByID(studentID);
		if (s == null) {
			return;
		}
		if (logToFile) {
			writer.println(University.STUDENT_BEGIN + s.getID() + " took an exam in course " + courseID + " and won "
					+ score + " points");
		}
		s.addScore(courseID, score);
	}

	/**
	 * Return the average score of a student
	 * 
	 * @param studentID
	 * @return average score of a student, -1 if student doesn't exist or has not
	 *         taken exams
	 */
	public String studentAvg(int studentID) {
		Student s = this.students.findByID(studentID);
		float avgScore = s.averageScore();
		if (avgScore != -1) {
			return University.STUDENT_BEGIN + s.getID() + " : " + avgScore;
		} else {
			return University.STUDENT_BEGIN + s.getID() + " hasn't taken any exams";
		}
	}

	public String courseAvg(int courseID) {
		Course c = this.courses.findByID(courseID);
		float avgScore = c.courseAvg();
		if (avgScore != -1) {
			return "The average for the course " + c.getTitle() + " is: " + avgScore;
		} else {
			return "No student has taken the exam in " + c.getTitle();
		}
	}

	/**
	 * Return the number of courses attende by a student
	 * 
	 * @param studentID
	 * @return number of attended courses
	 */
	public int courseAttended(int studentID) {
		int attended = 0;
		for (CustomObject c : this.courses) {
			Course tmp = (Course) c;
			attended += tmp.attendedByStudent(studentID) ? 1 : 0;
		}
		return attended;
	}

	/**
	 * Return the number of courses attende by a student
	 * 
	 * @param student
	 * @return number of attended courses
	 */
	public int courseAttended(Student s) {
		int attended = 0;
		for (CustomObject c : this.courses) {
			Course tmp = (Course) c;
			attended += tmp.attendedByStudent(s) ? 1 : 0;
		}
		return attended;
	}

	/**
	 * Return a student score considering the enrolled courses
	 * 
	 * @param studentID
	 * @return score
	 */
	private float studentScore(Student s) {
		float avgScore = s.averageScore();
		if (avgScore == -1) {
			return 0;
		}
		float bonus = ((float) s.numberOfExams() / this.courseAttended(s)) * 10;
		return avgScore + bonus;
	}

	/**
	 * Create an HashMap with all the students and their score
	 * 
	 * @return HashMap with students and scores
	 */
	private HashMap<Student, Float> scoreTable() {
		HashMap<Student, Float> table = new HashMap<>();

		for (CustomObject c : this.students) {
			Student s = (Student) c;
			table.put(s, this.studentScore(s));
		}
		return table;
	}

	/**
	 * Find the student with the highest score on the HashTable
	 * 
	 * @param HashTable
	 * @return Student
	 */
	private Map.Entry<Student, Float> getMaxStudent(HashMap<Student, Float> table) {
		float score;
		Map.Entry<Student, Float> maxKey = new AbstractMap.SimpleEntry<>(new Student(), new Float(0));
		for (Map.Entry<Student, Float> key : table.entrySet()) {
			score = key.getValue();
			if (score > maxKey.getValue()) {
				maxKey = key;
			}
		}
		return maxKey;
	}

	/**
	 * Find and print the top n students with the highest score in the University
	 */
	public String topNStudents(int n) {
		HashMap<Student, Float> table = scoreTable();
		Map.Entry<Student, Float> entry;
		Student s;
		StringBuilder bld = new StringBuilder();
		for (int i = 0; i < n; i++) {
			entry = getMaxStudent(table);
			s = entry.getKey();
			if (s.getName() != null) {
				table.remove(s);
				bld.append(s.fullName() + " : " + entry.getValue() + "\n");
			}
		}
		return bld.toString();
	}

	/**
	 * Find and print the top 3 students with the highest score in the University
	 */
	public String topThreeStudents() {
		return this.topNStudents(3);
	}
}
