package university;

abstract class CustomObject extends Object {
    private int id;

    /**
     * Getter for ID
     * 
     * @return ID
     */
    public int getID() {
        return this.id;
    }

    /**
     * Setter for ID
     * 
     * @param ID
     */
    public void setID(int id) {
        this.id = id;
    }

    /**
     * Return true if objects have the same ID
     * 
     * @param AnotherCustomObject
     * @return equals ids
     */
    public boolean equalsFromID(CustomObject c) {
        return c.getID() == this.getID();
    }

    /**
     * Return true if the object has this id
     * 
     * @param id
     * @return equals ids
     */
    public boolean equalsFromID(int id) {
        return id == this.getID();
    }
}