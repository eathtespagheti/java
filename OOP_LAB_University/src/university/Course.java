package university;

/**
 * This class rappresent a University Course
 * 
 * Manages the Course details and the enrolled Students
 */

public class Course extends CustomObject {
    private String title;
    private Person teacher;
    private StudentsList students = new StudentsList();

    /**
     * Constructor for course with teacher as string
     * 
     * @param id
     * @param title
     * @param teacher
     */
    public Course(int id, String title, String teacher) {
        this.setID(id);
        this.setTitle(title);
        this.setTeacher(teacher);
    }

    /**
     * Constructor for course without teacher
     * 
     * @param id
     * @param title
     */
    public Course(int id, String title) {
        this.setID(id);
        this.title = title;
    }

    /**
     * Getter for course title
     * 
     * @return course title
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * Setter for course title
     * 
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Getter for the teacher
     * 
     * @return Course teacher
     */
    public Person getTeacher() {
        return this.teacher;
    }

    /**
     * Setter for the teacher
     * 
     * @param teacher
     */
    public void setTeacher(Person teacher) {
        this.teacher = teacher;
    }

    /**
     * Setter for the teacher from string
     * 
     * @param teacher
     */
    public void setTeacher(String teacher) {
        String[] splitted = teacher.split(" ");
        String name = splitted[0];
        String surname = "";
        for (int i = 1; i < splitted.length; i++) {
            surname += splitted[i];
        }
        Person t = new Person(name, surname);
        this.setTeacher(t);
    }

    /**
     * Add a student to the course
     * 
     * @param student
     */
    public void addStudent(Student student) {
        this.students.addObject(student);
    }

    /**
     * Return a list of all the students
     * 
     * @return list fo students
     */
    public String studentsList() {
        return this.students.objectsList();
    }

    /**
     * Return if a given student attends the course
     * 
     * @param student
     * @return course attended
     */
    public Boolean containsStudent(Student student) {
        return this.students.contains(student);
    }

    /**
     * Return a human readable info about a course
     * 
     * @return id title rector
     */
    @Override
    public String toString() {
        return this.getID() + "," + this.getTitle() + "," + this.teacher.fullName();
    }

    /**
     * Return the student average course, returns -1 if no one has taken to course
     * 
     * @return average score or -1
     */
    public float courseAvg() {
        int sum = 0;
        int exams = 0;
        for (CustomObject student : this.students) {
            Student tmp = (Student) student;
            int score = tmp.getScore(this.getID());
            if (score != -1) {
                sum += score;
                exams++;
            }
        }
        return exams == 0 ? -1 : (float) sum / exams;
    }

    /**
     * Return true if a studend attend this course
     * 
     * @param studentID
     * @return course attended by student
     */
    public boolean attendedByStudent(int studentID) {
        Student s = this.students.findByID(studentID);
        return s != null;
    }

    /**
     * Return true if a studend attend this course
     * 
     * @param student
     * @return course attended by student
     */
    public boolean attendedByStudent(Student s) {
        return this.students.contains(s);
    }
}