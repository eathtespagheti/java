package university;

import java.io.IOException;
import java.util.logging.Logger;

public class UniversityExt extends University {
	private static final Logger logger = Logger.getLogger("University");

	public UniversityExt(String name) throws IOException {
		super(name);
		this.logToFile = false;
		// Example of logging
		logger.info("Creating extended university object");
	}

	/**
	 * Enroll a student in the university Return -1 if the maximum number of
	 * students it's reached
	 * 
	 * @param first first name of the student
	 * @param last  last name of the student
	 * @return student id
	 */
	@Override
	public int enroll(String first, String last) {
		int studentid = super.enroll(first, last);
		logger.info("New student enrolled: " + studentid + ", " + first + " " + last);
		return studentid;
	}

	/**
	 * Activates a new course with the given teacher
	 * 
	 * @param title   title of the course
	 * @param teacher name of the teacher
	 * @return the unique code assigned to the course
	 */
	@Override
	public int activate(String title, String teacher) {
		int courseID = super.activate(title, teacher);
		logger.info("New course activated: " + courseID + " " + title + " " + teacher);
		return courseID;
	}

	/**
	 * Register a student to attend a course
	 * 
	 * @param studentID  id of the student
	 * @param courseCode id of the course
	 */
	@Override
	public void register(int studentID, int courseCode) {
		super.register(studentID, courseCode);
		logger.info(University.STUDENT_BEGIN + studentID + " signed up for course " + courseCode);
	}

	/**
	 * Add a exam score
	 * 
	 * @param studentID
	 * @param courseID
	 * @param score
	 * @return true if successful, false if student doesn't exist or score it's
	 *         invalid
	 */
	@Override
	public void exam(int studentID, int courseID, int score) {
		super.exam(studentID, courseID, score);
		logger.info(University.STUDENT_BEGIN + studentID + " took an exam in course " + courseID + " and won " + score
				+ " points");
	}
}
