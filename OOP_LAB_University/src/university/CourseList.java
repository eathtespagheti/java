package university;

public class CourseList extends CustomObjectsList {
    /**
     *
     */
    private static final long serialVersionUID = -8890525445793560531L;

    /**
     * Getter for the number of enabled courses
     * 
     * @return number of enabled courses
     */
    public int getEnabledCourses() {
        return this.size();
    }

    /**
     * Add a course to the courses array
     *
     * @param id
     * @param title
     * @param teacher
     * @return course id
     */
    public int addCourse(int id, String title, String teacher) {
        Course tmp = new Course(id, title, teacher);
        return this.addObject(tmp);
    }

    /**
     * Print a list of courses
     */
    public void printCourses() {
        System.out.print(this.objectsList());
    }

    /**
     * Search the first course in the courses list by title and teacher. If none
     * it's found, return null
     * 
     * @param title
     * @param teacher
     * @return Fisrt matching course
     */
    public Course searchCourse(String title, String teacher) {
        for (Object obj : this) {
            Course course = (Course) obj;
            if (course.getTitle().equals(title)) {
                if (course.getTeacher().fullName().equals(teacher)) {
                    return course;
                }
            }
        }
        return null;
    }

    /**
     * Retrieves the study plan for a student
     * 
     * @param studentID id of the student
     * @return list of courses the student is registered for
     */
    public String studyPlan(Student s) {
        String list = "";
        for (CustomObject obj : this) {
            Course course = (Course) obj;
            if (course.containsStudent(s)) {
                list += course.toString() + "\n";
            }
        }
        return list;
    }

    @Override
    public Course findByID(int ID) {
        return (Course) super.findByID(ID);
    }
}
