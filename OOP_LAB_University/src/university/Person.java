package university;

/**
 * This class represents a person.
 * 
 * It manages his name and surname.
 *
 */
public class Person extends CustomObject {
    private String name;
    private String surname;

    /**
     * Empty constructor
     */
    public Person() {
    }

    /**
     * Constructor
     * 
     * @param name
     * @param surname
     */
    public Person(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    /**
     * Getter for the name of the Person
     * 
     * @return Person name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Setter for the name of the person
     * 
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter for the name of the Person
     * 
     * @return Person surname
     */
    public String getSurname() {
        return this.surname;
    }

    /**
     * Setter for the surname of the person
     * 
     * @param surname
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * Getter for the fullname of the person
     * 
     * @return Person name + surname
     */
    public String fullName() {
        return this.getName() + " " + this.getSurname();
    }
}
