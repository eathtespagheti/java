package university;

public class StudentsList extends CustomObjectsList {
	/**
	 *
	 */
	private static final long serialVersionUID = -7019741969931363459L;

	/**
	 * Getter for the number of enrolled students
	 * 
	 * @return number of enrolled students
	 */
	public int getEnrolledStudents() {
		return this.size();
	}

	/**
	 * Add a student to the students array
	 * 
	 * @param id
	 * @param name
	 * @param surname
	 * @return student id
	 */
	public int addStudent(int id, String name, String surname) {
		Student tmp = new Student(id, name, surname);
		return this.addObject(tmp);
	}

	/**
	 * Search the first student in the students array by name and surname. If none
	 * it's found, return null
	 * 
	 * @param name
	 * @param surname
	 * @return Fisrt matching Student
	 */
	public Student searchStudent(String name, String surname) {
		for (Object obj : this) {
			Student student = (Student) obj;
			if (student.getSurname().equals(surname)) {
				if (student.getName().equals(name)) {
					return student;
				}
			}
		}
		return null;
	}

	@Override
	public Student findByID(int ID) {
		return (Student) super.findByID(ID);
	}
}
