package university;

import java.util.ArrayList;

/**
 * This class rappresent an array of objects
 * 
 */

abstract class CustomObjectsList extends ArrayList<CustomObject> {
    /**
     *
     */
    private static final long serialVersionUID = -4545239268597225959L;

    /**
     * Add a CustomObject to the list and return the CustomObject ID
     * 
     * @param CustomObject
     * @return ID
     */
    public int addObject(CustomObject o) {
        this.add(o);
        return o.getID();
    }

    /**
     * Return a list of all the objects converted to string
     * 
     * @return objects list
     */
    public String objectsList() {
        String list = "";
        for (Object object : this) {
            list += object.toString() + "\n";
        }
        return list;
    }

    /**
     * Search and return for a CustomObject by ID, if not found return null
     * 
     * @param id
     * @return searchResult
     */
    public CustomObject findByID(int id) {
        for (CustomObject obj : this) {
            if (obj.equalsFromID(id)) {
                return obj;
            }
        }
        return null;
    }

    /**
     * Returns the index of the first occurrence of the specified element by id in
     * this list, or -1 if this list does not contain the element. More formally,
     * returns the lowest index i such that (o==null ? get(i)==null :
     * o.equals(get(i))), or -1 if there is no such index.
     * 
     * @param id
     * @return the index of the first occurrence of the specified element in this
     *         list, or -1 if this list does not contain the element
     */
    public int indexOf(int id) {
        int index = 0;
        for (CustomObject obj : this) {
            if (obj.equalsFromID(id)) {
                return index;
            }
            index++;
        }
        return -1;
    }
}