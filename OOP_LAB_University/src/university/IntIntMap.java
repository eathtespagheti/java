package university;

import java.util.HashMap;

public class IntIntMap extends HashMap<Integer, Integer> {

    /**
     *
     */
    private static final long serialVersionUID = -3491585644293839290L;

    /**
     * Return the average of all the values'
     * 
     * @return average of values', -1 if no score it's present
     */
    public float average() {
        int sum = 0;
        for (Integer value : this.values()) {
            sum += value;
        }
        return this.size() == 0 ? -1 : (float) sum / this.size();
    }
}