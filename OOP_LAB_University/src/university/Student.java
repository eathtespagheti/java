package university;

/**
 * This class rappresent a student, extension of a Person
 * 
 * Manage the ID of the student
 */
public class Student extends Person {
    private IntIntMap scores = new IntIntMap();

    protected Student() {
    }

    /**
     * Constructor for student
     * 
     * @param id
     * @param name
     * @param surname
     */
    public Student(int id, String name, String surname) {
        super(name, surname);
        this.setID(id);
    }

    /**
     * Return a string representing a student
     * 
     * @return id + fullname
     */
    @Override
    public String toString() {
        return "" + this.getID() + " " + this.fullName();
    }

    /**
     * Add a score for a course exam, if the course score it's already present it's
     * value will be updated
     * 
     * @param courseID
     * @param score
     * @return true it if the operation was successful, false it's the value it's
     *         not allowed
     */
    public boolean addScore(int courseID, int score) {
        if (score > 100 || score < 0) {
            return false;
        }
        this.scores.put(courseID, score);
        return true;
    }

    /**
     * Return the average score of the student
     * 
     * @return average score from all the course exams, -1 if no exam has been taken
     */
    public float averageScore() {
        return this.scores.average();
    }

    /**
     * Return the score realtive to a course exam, if not present return -1
     * 
     * @param courseID
     * @return score or -1
     */
    public int getScore(int courseID) {
        Integer score = this.scores.get(courseID);
        return score == null ? -1 : score;
    }

    /**
     * Return the number of exams attended
     * 
     * @return number of exams
     */
    public int numberOfExams() {
        return this.scores.size();
    }
}