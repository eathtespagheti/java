/**
 * 
 */
package it.polito.oop.books;

import java.util.List;

/**
 * @author Fabio Sussarellu s251489
 * @email fabio.sussarellu@studenti.polito.it
 *
 */
public abstract class Chapter {
	private String title;
	private int numPages;

	/**
	 * @param title
	 * @param text
	 * @param topics
	 * @param numPages
	 */
	public Chapter(String title, int numPages) {
		super();
		this.title = title;
		this.numPages = numPages;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the topics
	 */
	abstract List<Topic> getTopics();

	/**
	 * @return the numPages
	 */
	public int getNumPages() {
		return numPages;
	}

	/**
	 * @param numPages the numPages to set
	 */
	public void setNumPages(int numPages) {
		this.numPages = numPages;
	}
}
