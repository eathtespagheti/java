package it.polito.oop.books;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Book {
	private List<Topic> topics = new ArrayList<>();
	private List<Question> questions = new ArrayList<>();
	private List<TheoryChapter> theoryChapters = new ArrayList<>();
	private List<ExerciseChapter> exerciseChapter = new ArrayList<>();
	private List<Assignment> assignments = new ArrayList<>();

	/**
	 * Creates a new topic, if it does not exist yet, or returns a reference to the
	 * corresponding topic.
	 * 
	 * @param keyword the unique keyword of the topic
	 * @return the {@link Topic} associated to the keyword
	 * @throws BookException
	 */
	public Topic getTopic(String keyword) throws BookException {
		if (keyword == null || keyword.equals("")) {
			throw new BookException();
		}
		Optional<Topic> search = this.topics.parallelStream().filter(t -> t.getKeyword().equals(keyword)).findFirst();
		if (search.isPresent()) {
			return search.get();
		}
		Topic tmp = new Topic(keyword);
		this.topics.add(tmp);
		return tmp;
	}

	public Question createQuestion(String question, Topic mainTopic) {
		Question tmp = new Question(question, mainTopic);
		this.questions.add(tmp);
		return tmp;
	}

	public TheoryChapter createTheoryChapter(String title, int numPages, String text) {
		TheoryChapter tmp = new TheoryChapter(title, text, numPages);
		this.theoryChapters.add(tmp);
		return tmp;
	}

	public ExerciseChapter createExerciseChapter(String title, int numPages) {
		ExerciseChapter tmp = new ExerciseChapter(title, numPages);
		this.exerciseChapter.add(tmp);
		return tmp;
	}

	public List<Topic> getAllTopics() {
		return Stream.concat(this.theoryChapters.stream(), this.exerciseChapter.stream()).map(Chapter::getTopics)
				.flatMap(List::stream).distinct().sorted(Topic::compare).collect(Collectors.toList());
	}

	private <T extends Chapter> List<Topic> getTopics(List<T> l) {
		return l.stream().map(Chapter::getTopics).flatMap(List::parallelStream).distinct().collect(Collectors.toList());
	}

	public boolean checkTopics() {
		return this.getTopics(this.theoryChapters).containsAll(this.getTopics(this.exerciseChapter));
	}

	public Assignment newAssignment(String id, ExerciseChapter chapter) {
		Assignment tmp = new Assignment(id, chapter);
		this.assignments.add(tmp);
		return tmp;
	}

	/**
	 * builds a map having as key the number of answers and as values the list of
	 * questions having that number of answers.
	 * 
	 * @return
	 */
	public Map<Long, List<Question>> questionOptions() {
		MapBuilder map = new MapBuilder();
		this.questions.parallelStream().forEach(map::addQuestionToMap);
		return map.getMap();
	}
}
