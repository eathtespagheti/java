package it.polito.oop.books;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

public class Question {
	private String text;
	private Topic topic;
	private Map<String, Boolean> answers = new HashMap<>();

	/**
	 * @param topic the topic to set
	 */
	public void setTopic(Topic topic) {
		this.topic = topic;
	}

	/**
	 * @param question
	 */
	public Question(String question, Topic topic) {
		super();
		this.setQuestion(question);
		this.setTopic(topic);
	}

	/**
	 * @return the question
	 */
	public String getQuestion() {
		return this.text;
	}

	/**
	 * @param question the question to set
	 */
	public void setQuestion(String question) {
		this.text = question;
	}

	public Topic getMainTopic() {
		return this.topic;
	}

	public void addAnswer(String answer, boolean correct) {
		this.answers.put(answer, correct);
	}

	@Override
	public String toString() {
		return this.getQuestion() + " (" + this.getMainTopic() + ")";
	}

	public long numAnswers() {
		return this.answers.size();
	}

	public Set<String> getCorrectAnswers() {
		return this.answers.entrySet().parallelStream().filter(Entry::getValue).map(Entry::getKey)
				.collect(Collectors.toSet());
	}

	public Set<String> getIncorrectAnswers() {
		return this.answers.entrySet().parallelStream().filter(e -> !e.getValue()).map(Entry::getKey)
				.collect(Collectors.toSet());
	}
}
