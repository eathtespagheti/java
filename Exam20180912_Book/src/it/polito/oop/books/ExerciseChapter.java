package it.polito.oop.books;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class ExerciseChapter extends Chapter {
	private List<Question> questions = new LinkedList<>();

	/**
	 * @param title
	 * @param numPages
	 */
	public ExerciseChapter(String title, int numPages) {
		super(title, numPages);
	}

	public void addQuestion(Question question) {
		this.questions.add(question);
	}

	@Override
	public
	List<Topic> getTopics() {
		return this.questions.stream().map(Question::getMainTopic).distinct().sorted(Topic::compare)
				.collect(Collectors.toList());
	}
}
