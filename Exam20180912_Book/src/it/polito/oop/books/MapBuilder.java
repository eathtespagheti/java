/**
 * 
 */
package it.polito.oop.books;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Fabio Sussarellu s251489
 * @email fabio.sussarellu@studenti.polito.it
 *
 */
class MapBuilder {
	private Map<Long, List<Question>> map = new HashMap<>();

	public void addQuestionToMap(Question q) {
		if (this.map.containsKey(q.numAnswers())) {
			this.map.get(q.numAnswers()).add(q);
		} else {
			this.map.put(q.numAnswers(), Arrays.asList(q));
		}
	}

	/**
	 * @return the map
	 */
	public Map<Long, List<Question>> getMap() {
		return map;
	}

}
