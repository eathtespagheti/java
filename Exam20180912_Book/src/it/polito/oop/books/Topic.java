package it.polito.oop.books;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Topic {
	private String keyword;
	private List<Topic> topics = new LinkedList<>();

	/**
	 * @param keyword
	 */
	public Topic(String keyword) {
		super();
		this.setKeyword(keyword);
	}

	/**
	 * @return the keyword
	 */
	public String getKeyword() {
		return keyword;
	}

	/**
	 * @param keyword the keyword to set
	 */
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	@Override
	public String toString() {
		return this.getKeyword();
	}

	public boolean addSubTopic(Topic topic) {
		if (this.topics.contains(topic)) {
			return false;
		}
		this.topics.add(topic);
		return true;
	}

	/*
	 * Returns a sorted list of subtopics. Topics in the list *MAY* be modified
	 * without affecting any of the Book topic.
	 */
	public List<Topic> getSubTopics() {
		return this.topics.stream().sorted((a, b) -> a.getKeyword().compareTo(b.getKeyword()))
				.collect(Collectors.toList());
	}

	static int compare(Topic o1, Topic o2) {
		return o1.getKeyword().compareTo(o2.getKeyword());
	}

	protected List<Topic> getAllTopicsRecursive() {
		if (this.topics.isEmpty()) {
			return this.topics;
		}
		List<Topic> collection = new LinkedList<>();
		collection.addAll(this.topics);
		this.topics.forEach(t -> collection.addAll(t.getAllTopicsRecursive()));
		collection.add(this);
		return collection;
	}
}
