package it.polito.oop.books;

import java.util.List;

public class Assignment {
	private String id;
	private ExerciseChapter chapter;
	private double score = 0;

	/**
	 * @param id
	 * @param chapter
	 */
	public Assignment(String id, ExerciseChapter chapter) {
		super();
		this.id = id;
		this.chapter = chapter;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the exercise
	 */
	public ExerciseChapter getChapter() {
		return chapter;
	}

	/**
	 * @param chapter the chapter to set
	 */
	public void setChapter(ExerciseChapter chapter) {
		this.chapter = chapter;
	}

	public double addResponse(Question q, List<String> answers) {
		int corrette = 0;
		int sbagliate = 0;
		for (String answer : answers) {
			if (q.getCorrectAnswers().contains(answer)) {
				corrette++;
			}
			if (q.getIncorrectAnswers().contains(answer)) {
				sbagliate++;
			}
		}
		int corretteNonFornite = q.getCorrectAnswers().size() - corrette;
		double punteggio = (double) (q.numAnswers() - sbagliate - corretteNonFornite) / q.numAnswers();
		this.score += punteggio;
		return punteggio;
	}

	public double totalScore() {
		return this.score;
	}

}
