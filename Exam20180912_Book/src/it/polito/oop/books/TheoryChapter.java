package it.polito.oop.books;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class TheoryChapter extends Chapter {
	private String text;
	private List<Topic> topics = new LinkedList<>();

	/**
	 * @param title
	 * @param text
	 * @param topics
	 * @param numPages
	 */
	public TheoryChapter(String title, String text, int numPages) {
		super(title, numPages);
		this.setText(text);
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @return the topics
	 */
	public List<Topic> getTopics() {
		return this.topics.stream().flatMap(t -> t.getAllTopicsRecursive().stream()).distinct().sorted(Topic::compare)
				.collect(Collectors.toList());
	}

	private void addTopicIfNotPresent(Topic topic) {
		if (!this.getTopics().contains(topic)) {
			this.topics.add(topic);
		}
	}

	public void addTopic(Topic topic) {
		this.addTopicIfNotPresent(topic);
		topic.getSubTopics().parallelStream().forEach(this::addTopicIfNotPresent);
	}

}
